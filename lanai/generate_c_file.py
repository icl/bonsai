#!/usr/bin/env python
import subprocess as subp
import sys
if (sys.version_info > (3, 0)):
    from . import modify_c_file as mc
else:
    import modify_c_file as mc


def generate_c_file(iterator, c_file, graphviz=False, omp=None):
    gen_statement = "python lanai/lanai.py --generate "
    if graphviz:
        gen_statement += "--graphviz " + c_file.split(".")[0] + ".dot "

    if omp:
        gen_statement += "--omp "
    gen_statement += "-o " + c_file + " " + iterator
    rv = subp.call(gen_statement, shell=True)
    if rv != 0:
        raise SyntaxError

    mc.mod_file_for_printing(c_file, iterator, omp)
    return
