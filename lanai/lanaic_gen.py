#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set fileencoding=utf-8 :

from __future__ import print_function

import compiler
import os
import sys

import lanai
import lanaic_ast


class AbstractSyntaxTree:
    def __init__(self, fname):
        self.fname = fname
        # self.ast = ast.parse(open(fname).read(), fname, mode="exec")
        self.main_module = compiler.parse(open(fname).read())

    def __iter__(self):
        return ModuleIterator(self.main_module)


class ParseContext:

    def __init__(self, script_filename, filename):
        self.script_filename = script_filename
        self.filename = filename

        self.the_globals = dict()
        self.the_locals = dict()

        for path in (os.path.dirname(os.path.abspath(self.filename)),
                     os.path.split(os.path.dirname(
                         os.path.abspath(self.script_filename)))[0]):
            # if 'path' not already inserted by user
            # through shell environment (PYTHONPATH)
            if path not in sys.path:
                # print(path)
                sys.path.append(path)
            break

    def __str__(self):
        ss = ""
        for attr in "the_globals", "the_locals":
            ss += "%s:\n" % attr.upper().replace("_", " ")
            for k in getattr(self, attr):
                ss += " " + k + " -> " + repr(self.the_globals) + "\n"

        return ss

    def parse_expr(self, node):
        full_class_name = str(node.__class__)
        comp_ast_mod = "compiler.ast."
        if full_class_name.startswith(comp_ast_mod):
            return getattr(self, "parse_" +
                           full_class_name[len(comp_ast_mod):].lower())(node)

        elif full_class_name.startswith("<type '"):  # builtin types
            return node

        else:
            raise ValueError, "Unknown class " + full_class_name + \
                              " for object " + repr(node)

    def parse_add(self, node):
        return self.parse_expr(node.left) + self.parse_expr(node.right)

    def parse_assign(self, node):
        expr = self.parse_expr(node.expr)
        for node1 in node.nodes:
            if isinstance(node1, compiler.ast.AssName) \
                    and node1.flags == "OP_ASSIGN":

                if type(expr) is list:
                    self.the_globals[node1.name] = \
                        lanai.IteratorNoDepList(expr, node1.name)
                else:
                    self.the_globals[node1.name] = expr
                if hasattr(expr, "set_parse_name"):
                    # try to set the name (in case 'expr' is NamedExpression)
                    expr.set_parse_name(node1.name)

            else:
                raise ValueError, "Cannot parse: " \
                                  + str(node1.__class__) \
                                  + str(node1.flags)

    def parse_callfunc(self, node):
        assert node.star_args is None and node.dstar_args is None
        func = self.parse_expr(node.node)
        args = [self.parse_expr(arg) for arg in node.args]
        return func(*args)

    def parse_compare(self, node):
        expr0 = self.parse_expr(node.expr)
        for op, expr_ast in node.ops:
            expr = self.parse_expr(expr_ast)
            if op == "==":
                if not expr0 == expr:
                    return False
            elif op == "!=":
                if not expr0 != expr:
                    return False
            elif op == "<=":
                if not expr0 <= expr:
                    return False
            elif op == "<":
                if not expr0 < expr:
                    return False
            elif op == ">=":
                if not expr0 >= expr:
                    return False
            elif op == ">":
                if not expr0 > expr:
                    return False
            expr0 = expr
        return True

    def parse_const(self, node):
        return node.value

    def parse_discard(self, node):
        raise ValueError, "Unimplemented"

    def parse_div(self, node):
        return self.parse_expr(node.left) / self.parse_expr(node.right)

    def parse_from(self, node):
        def import_names(names, vdict):
            for src, alias in names:
                # don't import double underscore names
                if src.startswith("__"):
                    continue

                if alias is None:
                    alias = src
                # don't import as double underscore names
                if alias.startswith("__"):
                    raise ValueError, \
                        "Are you sure you want to import as double underscore"\
                        " names? '%s' as '%s'." % (str(src), str(alias))

                if src == "*":
                    import_names([(n, None) for n in dir(mod)],
                                 self.the_globals)

                else:
                    self.the_globals[alias] = getattr(mod, src)

        if node.modname.startswith("lanai."):
            # we can do something special for BONSAI and LANAI packages
            pass  # return

        from_list = map(lambda t: t[0], node.names)  # extract names from the list of tuples "node.names"
        mod = __import__(node.modname, self.the_globals, self.the_locals,
                         from_list, -1)  # does not work for imports inside a package with "-m lanai"

        import_names(node.names, self.the_globals)

        return

        print(node.modname, node.names)
        import imp
        filename, pathname, description = imp.find_module(nm)
        imp.load_module(name, filename, pathname, description)

    def parse_function(self, node):
        if node.decorators is None or len(node.decorators.nodes) > 1:
            raise ValueError, "Not implemented for decorators " + \
                              str(node.decorators) + " in function "\
                              + node.name

        self.the_globals[node.name] = \
            self.parse_expr(node.decorators.nodes[0])(None, node)
        self.the_globals[node.name].set_parse_name(node.name)

    def parse_getattr(self, node):
        return getattr(self.parse_expr(node.expr),
                       self.parse_expr(node.attrname))

    def parse_if(self, node):
        for expr, stmt in node.tests:
            if self.parse_expr(expr):
                self.parse_stmt(stmt)
                return

        if node.else_:
            self.parse_stmt(node.else_)

    def parse_list(self, node):
        return [self.parse_expr(x) for x in node.nodes]

    def parse_module(self, node):
        self.parse_stmt(node.node)

    def parse_mul(self, node):
        return self.parse_expr(node.left) * self.parse_expr(node.right)

    def parse_name(self, node):
        return self.the_globals[node.name]

    def parse_stmt(self, node):
        comp_ast_mod = "compiler.ast."
        for nn in node.nodes:
            full_class_name = str(nn.__class__)
            if full_class_name.startswith(comp_ast_mod):
                getattr(self, "parse_" +
                        full_class_name[len(comp_ast_mod):].lower())(nn)
            else:
                raise ValueError, "Unknown class " + full_class_name

    def parse_subscript(self, node):
        assert node.flags == "OP_APPLY"
        expr = self.parse_expr(node.expr)
        if len(node.subs) <= 1:
            return expr[self.parse_expr(self.parse_expr(node.subs[0]))]
        else:
            raise ValueError

    def parse_unaryadd(self, node):
        return self.parse_expr(node.expr)

    def parse_unarysub(self, node):
        return -self.parse_expr(node.expr)


def parse_file(script_filename, filename):
    context = ParseContext(script_filename, filename)

    tree = AbstractSyntaxTree(filename)
    context.parse_module(tree.main_module)

    return tree, context


def unique(l):
    d = dict()
    newl = list()
    for e in l:
        if not d.has_key(e):
            newl.append(e)
            d[e] = 1
    return newl


def dep_filter(context, names):
    """
    Return the list of meaningful dependences.
    """
    l = list()
    for name in names:
        obj = context.the_globals[name]
        if isinstance(obj, (lanai.Iterator, lanai.IteratorFunction,
                            lanai.ConditionFunction)):
            l.append(name)
        elif isinstance(obj, lanai.IteratorExpression):
            l.extend(obj.get_deps())
    return unique(l)


def show_iterators(context):
    iterators = dict()
    conditions = dict()
    for name in context.the_globals:
        obj = context.the_globals[name]
        if isinstance(obj, lanai.Iterator):
            iterators[name] = obj
            print("ITER", name, obj.get_deps())

        elif isinstance(obj, lanai.IteratorExpression):
            print("EXPR", name, dep_filter(context, obj.get_deps()))

        elif isinstance(obj, lanai.IteratorFunction):
            iterators[name] = obj
            print("FUNC", name, dep_filter(context,
                                           obj.get_deps()))
            # filter(lambda x: isinstance(context.the_globals[x],
            #                             lanai.Iterator), obj.get_deps()))

        elif isinstance(obj, lanai.ConditionFunction):
            conditions[name] = obj
            print("COND", name, dep_filter(context, obj.get_deps()))


def get_dep_levels(context):
    """
    Build dependence levels.
    
    Each level of dependences has objects that depend on only the prior levels.
    """

    iterators = dict()  # analyzed iterators
    levels = list()  # list of levels, each level only depends on prior ones
    has_more = True
    leftover = list()
    while has_more:
        # 'if' statements below will determine if there is
        # more iterators to process
        has_more = False
        levels.append(list())  # start with empty level at the end
        del leftover[:]  # clear leftovers from the previous iteration
        for name in context.the_globals:
            if iterators.has_key(name):  # already analyzed?
                continue

            obj = context.the_globals[name]

            if isinstance(obj,
                          (lanai.Iterator, lanai.IteratorFunction,
                           lanai.ConditionFunction, lanai.NamedExpression)):
                deps = dep_filter(context, obj.get_deps())
                for d in deps:  # check if all dependences are satisfied
                    if not iterators.has_key(d):
                        # print(len(levels), d, name, deps, levels)
                        leftover.append(name)
                        has_more = True
                        break
                else:
                    levels[-1].append(name)  # add it to the current level

        # this is the time to add the objects from the current level
        # to the "resolved" iterators
        for name in levels[-1]:  # mark new iterators/conditions as resolved
            iterators[name] = 1

        # last level is empty - something must have gone wrong
        if len(levels[-1]) == 0:
            print(leftover)
            for name in context.the_globals:
                if iterators.has_key(name):  # already analyzed?
                    continue
                if isinstance(obj, (lanai.Iterator, lanai.IteratorFunction,
                                    lanai.ConditionFunction)):
                    print(name, deps, levels)
            raise ValueError, "Empty last level (probably unsatisfied " \
                              "dependences): " + str(levels)

    return levels


def append_intrinsics(stmt):
    func = lanaic_ast.Function("min",
                               (lanaic_ast.Decl("x", lanaic_ast.Int()),
                                lanaic_ast.Decl("y", lanaic_ast.Int())),
                               lanaic_ast.Int(),
                               lanaic_ast.Stmt([]), ("static",))
    func.stmt.append(
        lanaic_ast.Return(
            lanaic_ast.TernaryOp(lanaic_ast.BinOp(
                "<", lanaic_ast.Name("x"), lanaic_ast.Name("y")),
                lanaic_ast.Name("x"), lanaic_ast.Name("y"))))
    stmt.append(func)


def sort_level(dep_level, context):
    d = {}
    count = 0
    for name in dep_level:
        obj = context.the_globals[name]
        if isinstance(obj, lanai.IteratorNoDepList):
            d[name] = len(obj.values)
            count += 1
        elif isinstance(obj, lanai.Iterator) and not isinstance(obj.past_last, lanai.IteratorExpression):
            d[name] = obj.past_last / obj.step
            count += 1
        else:
            d[name] = -1
    rv = sorted(d, key=d.get, reverse=True)
    return rv, count


def generate_c(context, omp, file_pointer=sys.stdout):
    # FIXME: add if (check if any iterations) and switch for() from < to !=
    cf = lanaic_ast.C_File(file_pointer)
    cf.stmt.append(lanaic_ast.Include("math.h"))
    cf.stmt.append(lanaic_ast.Include("stdio.h"))
    if omp:
        cf.stmt.append(lanaic_ast.Include("omp.h"))

    append_intrinsics(cf.stmt)
    iterate_func = "iterate"
    func = lanaic_ast.Function(iterate_func,
                               (lanaic_ast.Decl("idx0", lanaic_ast.Int()),),
                               lanaic_ast.Int(),
                               lanaic_ast.Stmt([]), ("static",))
    cf.stmt.append(func)
    loop_body = func.stmt
    # pprint.pprint(loop_body);
    levels = get_dep_levels(context)
    # sort the first level (the first Loop Nest Construct) by the iteration count
    # to maximize the utility of using a "collapse"
    levels[0], collapse_count = sort_level(levels[0], context)
    '''FIXME: need to remove placeholders used for existing wrappers 
    and incorporate pragma insertion into AST'''

    if omp:
        loop_body.append(lanaic_ast.Decl("omp_pragma", lanaic_ast.Int(),))

    for level in levels:  # start from the outermost level and move inward
        for sweep in range(3):
            for name in level:
                obj = context.the_globals[name]
                if sweep == 0:
                    # go through iterator expressions first to have them
                    # calculated for conditions and iterators
                    if not isinstance(obj, lanai.IteratorExpression):
                        continue
                elif sweep == 1:
                    # go through conditional expressions next for more
                    # efficient execution conditions will go in outer scopes
                    #  to be triggered early
                    if not isinstance(obj, lanai.ConditionFunction):
                        continue
                else:
                    if isinstance(obj, (lanai.IteratorExpression,
                                        lanai.ConditionFunction)):
                        continue

                loop_body = obj.codegen(func, loop_body, context)

    loop_body.append(lanaic_ast.Assign("+=", lanaic_ast.Name("idx"),
                                       lanaic_ast.Const(1)))

    #func.stmt.insert(0, lanaic_ast.Assign("=", lanaic_ast.Decl("idx", lanaic_ast.VolatileInt()), lanaic_ast.Name("idx0")))

    # FIXME: remove placeholder and incorporate print insertion into AST
    loop_body.append(lanaic_ast.Assign("=",
                                       lanaic_ast.Name("print_placeholder"),
                                       lanaic_ast.Const(1)))
    func.stmt.append(lanaic_ast.Return(lanaic_ast.Name("idx")))
    cf.stmt.append(
        lanaic_ast.Function("main",
                            (lanaic_ast.Decl("argc", lanaic_ast.Int()),
                             lanaic_ast.Decl("argv", lanaic_ast.ArgvArray()), ),
                            lanaic_ast.Int(),
                            lanaic_ast.Stmt(
                                [lanaic_ast.CallFunc(iterate_func,
                                                     (lanaic_ast.Name("argc"),)
                                                     ),
                                 lanaic_ast.Return(lanaic_ast.Const(0))])
                            )
    )
    cf.save()
    return levels


def generate_dot(graphviz, tree, context):
    fobj = open(graphviz, "w")
    fobj.write("digraph G {\n")
    fobj.write("rankdir=TB;\n")

    # find important objects: iterators, conditions, or named expressions
    nodes = dict()
    dep_on = dict()
    dep_by = dict()
    for name in context.the_globals:
        obj = context.the_globals[name]
        if isinstance(obj, (lanai.Iterator, lanai.IteratorFunction,
                            lanai.ConditionFunction)) \
                or isinstance(obj, lanai.IteratorExpression) \
                and hasattr(obj, "name"):
            nodes[name] = 1
            dep_on[name] = dict()
            dep_by[name] = dict()

    for name in nodes:
        obj = context.the_globals[name]
        for dep in dep_filter(context, obj.get_deps()):
            if nodes.has_key(dep):  # only look at interesting objects
                if not dep_on[name].has_key(dep):
                    fobj.write("%s -> %s;\n" % (name, dep))
                dep_on[name][dep] = 1
                dep_by[dep][name] = 1

    in_levels = dict()
    levels = list()
    lvl = list()
    for name in nodes:
        if len(dep_on[name]) == 0 and not in_levels.has_key(name):
            lvl.append(name)
            in_levels[name] = 1

    while True:
        levels.append(lvl)

        lvl = list()
        for name in levels[-1]:
            for revdep in dep_by[name]:
                del dep_on[revdep][name]
                # if all depences satisfied and not in levels already
                if len(dep_on[revdep]) == 0 and not in_levels.has_key(revdep):
                    lvl.append(revdep)
                    in_levels[revdep] = 1
        if len(lvl) < 1:
            break

    for lvl in levels:
        fobj.write("{ rank=same; ")
        for name in sorted(lvl):
            fobj.write(" %s;" % name)
        fobj.write("}\n")

    fobj.write("}\n")
    fobj.close()
    return levels


def process_file(script_filename,
                 filename,
                 graphviz, omp=None,
                 file_pointer=sys.stdout):
    tree, ctxt = parse_file(script_filename, filename)
    levels = generate_c(ctxt, omp, file_pointer)
    # show_iterators(ctxt) #c
    if graphviz:
        import pprint
        # print("/*\n")
        # pprint.pprint(map(sorted, levels))
        # print(map(sorted, levels))
        # print("*/\n")

        levels = generate_dot(graphviz, tree, ctxt)
        # print("/*\n")
        # pprint.pprint(map(sorted, levels))
        # print(map(sorted, levels))
        # print("*/\n")
    return tree, ctxt, levels



def main(argv):
    import argparse

    argparser = argparse.ArgumentParser(
        description="Parse LANAI iterators' description and "
                    "generate code for it.")
    argparser.add_argument("--generate",
                           action="store_true",
                           help="Generate the code from LANAI iteration file.")
    argparser.add_argument("--omp", action="store_true",
                           help="Include OpenMP support")
    argparser.add_argument("--graphviz",
                           action="store", type=str, nargs=1,
                           metavar="DOT_FILENAME",
                           help="Generate dependence graph file for "
                                "rendering with GraphViz.")
    argparser.add_argument("filename", metavar="FILENAME.py", type=str,
                           nargs=1, help="File name of the iteration file.")
    argparser.add_argument("-o", "--output", type=str, nargs=1,
                           help="output filename")

    args = argparser.parse_args(argv[1:])

    graphviz = None
    omp = None
    if args.omp:
        omp = args.omp
    if args.graphviz:
        graphviz = args.graphviz[0]
    if args.output:
        fp = open(args.output[0], "w+")
    else:
        fp = sys.stdout

    process_file(argv[0], args.filename[0], graphviz, omp, file_pointer=fp)

    if args.output:
        fp.close()

    return 0


if "__main__" == __name__:
    sys.exit(main(sys.argv))

# Local Variables:
# mode: python
# tab-width: 4
# indent-tabs-mode: nil
# fill-column: 79
# End:
