#!/usr/bin/env python
import argparse
import sys
import lanai_wrappers as lw
import generate_c_file as gen


def main(argv):
    parser = argparse.ArgumentParser(description="Runs the LANAI compiler with the specified input and output to "
                                                 "generate possible valid configurations.")
    parser.add_argument("--graphviz", action="store", type=str, nargs=1, metavar="DOT_FILENAME",
                        help="Generate dependence graph file for rendering with GraphViz.")
    parser.add_argument("--noquiet", action="store_true", help="Keep intermediate files. (C file only for now)")
    parser.add_argument("-c", "--cc", action="store", type=str, nargs='?', metavar="COMPILER", help="Specify different compiler. Defaults to 'cc'")
    parser.add_argument("iterator", metavar="ITERATORS.py", type=str, nargs=1,
                        help="File containing the iterator specifications.")
    parser.add_argument("-o", "--outfile", metavar="OUTFILE.csv", type=str, nargs='?',
                        help="name of CSV output file holding valid configurations")
    parser.add_argument("-p", "--omp", action="store_true", help="Use OpenMP")

    args = parser.parse_args(argv[1:])

    file_base = args.iterator[0].split('.')[0]

    cfile_name = file_base + ".c"

    if args.outfile is None:
        csv_name = file_base + ".csv"
    else:
        csv_name = args.outfile[0]

    if args.cc is None:
        cc = "cc"
    else:
        cc = args.cc

    # Best option for now
    #lw.generate_c_file(args, cfile_name)
    gen.generate_c_file(args.iterator[0], cfile_name, args.graphviz, args.omp)
    lw.generate_csv(args, file_base, cfile_name, csv_name, cc, args.omp)


if __name__ == '__main__':
    sys.exit(main(sys.argv))
