#!/usr/bin/env python
import sys
import os
import subprocess as subp
import csv
import shutil
from tempfile import NamedTemporaryFile


def reindex_file(csvf):
    tempfile = NamedTemporaryFile(delete=False)
    with open(csvf, 'rb') as csvfile, tempfile:
        reader = csv.reader(csvfile, delimiter=',')
        writer = csv.writer(tempfile, delimiter=',')
        row = next(reader)
        writer.writerow(row)

        # necessary for 0 indexing the configurations
        for row in reader:
            row[-1] = int(row[-1]) - 2
            writer.writerow(row)

    shutil.move(tempfile.name, csvf)
    return


def generate_csv(conf, c_file, csv_file):
    exe_file = c_file.split(".")[0]
    cc_statement = " ".join([conf["lanai_cc"],
                             conf["lanai_flags"],
                             "-o",
                             exe_file,
                             c_file])
    if conf["openmp"]:
        cc_statement += " -fopenmp"

    print(cc_statement)
    p = subp.call(cc_statement, shell=True)
    if p != 0:
        raise RuntimeError("File %s did not compile" % c_file)

    ex_statement = exe_file + " > " + csv_file
    print("Generating space. Please wait.")
    p = subp.call(ex_statement, shell=True)
    if p != 0:
        raise RuntimeError("Error in executing %s" % exe_file)
    if conf["openmp"]:
        subp.call("rm " + c_file, shell=True)

    # reindex the csv file for 0 indexing
    reindex_file(csv_file)

    print("Done.")
    return
