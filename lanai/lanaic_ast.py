#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set fileencoding=utf-8 :

"""
Classes for dynamic Abstract Syntax Tree that is built at
runtime during code generation.
"""

INDENT = 2


def save_indent(fobj, indent):
    fobj.write(" " * indent)


class C_File:
    def __init__(self, fobj):
        self.fobj = fobj
        self.stmt = Stmt([])

    def save(self, indent=0):
        self.stmt.save(self.fobj, indent)


class BuiltinDtype:
    def save(self, fobj, indent):
        save_indent(fobj, indent)
        fobj.write(self.name)


class Assign:
    def __init__(self, op, lvalue, rvalue):
        self.op = op
        self.lvalue = lvalue
        self.rvalue = rvalue

    def save(self, fobj, indent):
        save_indent(fobj, indent)
        self.lvalue.save(fobj, indent)
        fobj.write(" %s " % self.op)
        self.rvalue.save(fobj, 0)


class BinOp:
    def __init__(self, op, left, right):
        self.op = op
        self.left = left
        self.right = right

    def save(self, fobj, indent):
        if isinstance(self.left, BinOp):
            fobj.write("(")
            self.left.save(fobj, 0)
            fobj.write(")")
        else:
            self.left.save(fobj, indent)
        fobj.write(" ")
        fobj.write(self.op)
        fobj.write(" ")
        if isinstance(self.right, BinOp):
            fobj.write("(")
            self.right.save(fobj, 0)
            fobj.write(")")
        else:
            self.right.save(fobj, 0)


class Break:
    def __init__(self): pass

    def save(self, fobj, indent):
        save_indent(fobj, indent)
        fobj.write("break")


class CallFunc:
    def __init__(self, name, args):
        self.name = name
        self.args = args

    def save(self, fobj, indent):
        save_indent(fobj, indent)
        fobj.write(self.name)
        fobj.write("(")
        beyond_first = False
        for arg in self.args:
            if beyond_first:
                fobj.write(", ")
            arg.save(fobj, 0)
            beyond_first = True
        fobj.write(")")


class Const:
    def __init__(self, value):
        self.value = value

    def save(self, fobj, indent):
        fobj.write(str(self.value))


class CompoundInitializer:
    def __init__(self, *values):
        self.values = values

    def save(self, fobj, indent):
        fobj.write("{")
        sep = ""
        for v in self.values:
            fobj.write(sep)
            v.save(fobj, 0)
            sep = ", "
        fobj.write("}")


class Continue:
    def save(self, fobj, indent):
        save_indent(fobj, indent)
        fobj.write("continue")


class Decl:
    def __init__(self, name, dtype, dimension=None, value=None):
        """
        name is the C name of the variable to declare

        dtype is the C data type to use for declaration

        dimension is the dimension of the declared name or None
        if it's not an array

        value is the value to use for initializing the variable
        in the declaration
        """
        self.name = name
        self.dtype = dtype
        self.value = value
        self.dimension = dimension

    def save(self, fobj, indent):
        self.dtype.save(fobj, indent)
        fobj.write(" ")
        fobj.write(self.name)
        if self.dimension is not None:
            fobj.write("[%d]" % self.dimension)
        if self.value:
            fobj.write(" = ")
            self.value.save(fobj, 0)


class DoWhile:
    def __init__(self, test, body):
        self.test = test
        self.body = body

    def save(self, fobj, indent):
        save_indent(fobj, indent)
        fobj.write("do\n")
        self.body.save(fobj, indent+INDENT)
        save_indent(fobj, indent)
        fobj.write("while (")
        self.test.save(fobj, 0)
        fobj.write(")")


class Function:
    def __init__(self, name, args, return_dtype, stmt, attributes=None):
        self.name = name
        self.args = args
        self.return_dtype = return_dtype
        self.stmt = stmt
        self.attributes = attributes
        self.declarations = dict()

    def declare(self, node):
        if not self.declarations.has_key(node.name):
            self.declarations[node.name] = 1
            self.stmt.insert(0, node)

    def save(self, fobj, indent):
        save_indent(fobj, indent)
        if self.attributes:
            for i, attr in enumerate(self.attributes):
                if i > 0:
                    fobj.write(" ")
                fobj.write(attr)
            fobj.write(" ")
        self.return_dtype.save(fobj, indent)
        fobj.write(" ")
        save_indent(fobj, indent)
        fobj.write(self.name)
        fobj.write("(")
        for i, arg in enumerate(self.args):
            if i > 0:
                fobj.write(", ")
            arg.save(fobj, 0)
        fobj.write(")\n")
        self.stmt.save_compound(fobj, indent+INDENT)


class For:
    def __init__(self, first, condition, increment, body):
        self.first = first
        self.condition = condition
        self.increment = increment
        self.body = body

    def save(self, fobj, indent):
        save_indent(fobj, indent)
        fobj.write("for (")
        beyond_first = False
        for node in (self.first, self.condition, self.increment):
            if beyond_first:
                fobj.write("; ")
            node.save(fobj, 0)
            beyond_first = True
        fobj.write(")\n")
        self.body.save(fobj, indent+INDENT)


class Pragma:
    def __init__(self, clauses):
        self.clauses = clauses

    def save(self, fobj, indent):
        save_indent(fobj, indent)
        fobj.write("#pragma omp ")
        fobj.write(self.clauses)
        fobj.write("\n")


class Name:
    def __init__(self, name):
        self.name = name

    def save(self, fobj, indent):
        fobj.write(self.name)


class If:
    def __init__(self, tests, else_):
        self.tests = tests
        self.else_ = else_

    def save(self, fobj, indent):
        save_indent(fobj, indent)
        beyond_first = False
        for expr, stmt in self.tests:
            if beyond_first:
                fobj.write("else ")
            fobj.write("if (")
            expr.save(fobj, 0)
            fobj.write(")\n")
            stmt.save(fobj, indent+INDENT)
            beyond_first = True
        if self.else_ is not None:
            save_indent(fobj, indent)
            fobj.write("else\n")
            self.else_.save(fobj, indent+INDENT)

    def __str__(self):
        return "If(" + str(self.tests) + "," + str(self.else_) + ")"

    def __repr__(self):
        return "If(" + repr(self.tests) + "," + repr(self.else_) + ")"


class Include:
    def __init__(self, filename, system=True):
        self.filename = filename
        self.system = system

    def save(self, fobj, indent):
        save_indent(fobj, indent)
        if self.system:
            p = "<>"
        else:
            p = '""'
        fobj.write("#include %s%s%s" % (p[0], self.filename, p[1]))


class Int(BuiltinDtype):
    name = "int"


class VolatileInt(BuiltinDtype):
    name = "int volatile"


class Return:
    def __init__(self, value):
        self.value = value

    def save(self, fobj, indent):
        save_indent(fobj, indent)
        fobj.write("return ")
        self.value.save(fobj, 0)


class Stmt:
    def __init__(self, nodes):
        self.nodes = nodes

    def append(self, node):
        self.nodes.append(node)

    def insert(self, i, node):
        self.nodes.insert(i, node)

    def save_one(self, node, fobj, indent):
        node.save(fobj, indent)
        if not isinstance(node, (For, Function, If, Include)):
            fobj.write(";\n")
        if isinstance(node, Include):
            fobj.write("\n")

    def save(self, fobj, indent):
        if len(self.nodes) == 0:
            save_indent(fobj, indent)
            fobj.write("{}\n")
        elif len(self.nodes) == 1:
            self.save_one(self.nodes[0], fobj, indent)
        else:
            self.save_compound(fobj, indent)

    def save_compound(self, fobj, indent):
        if indent > 0:
            save_indent(fobj, indent-INDENT)
            fobj.write("{\n")
            new_indent = indent
        else:
            new_indent = 0
        for node in self.nodes:
            self.save_one(node, fobj, new_indent)
        if indent > 0:
            save_indent(fobj, max(indent-INDENT, 0))
            fobj.write("}\n")

    def __str__(self):
        return "Stmt(" + str(self.nodes) + ")"

    def __repr__(self):
        return "Stmt(" + repr(self.nodes) + ")"


class TernaryOp:
    def __init__(self, condition, if_true, if_false):
        self.condition = condition
        self.if_true = if_true
        self.if_false = if_false

    def save(self, fobj, indent):
        save_indent(fobj, indent)
        self.condition.save(fobj, 0)
        fobj.write(" ? ")
        self.if_true.save(fobj, 0)
        fobj.write(" : ")
        self.if_false.save(fobj, 0)


class UnaryOp:
    def __init__(self, op, expr):
        self.op = op
        self.expr = expr

    def save(self, fobj, indent):
        save_indent(fobj, indent)
        fobj.write(self.op)
        self.expr.save(fobj, 0)


class IndexOp:
    def __init__(self, expr, index):
        self.expr = expr
        self.index = index

    def save(self, fobj, indent):
        save_indent(fobj, indent)
        self.expr.save(fobj, 0)
        fobj.write("[")
        self.index.save(fobj, 0)
        fobj.write("]")



class Void(BuiltinDtype):
    name = "void"


class ArgvArray(BuiltinDtype):
    name = "char**"


# Local Variables:
# mode: python
# tab-width: 4
# indent-tabs-mode: nil
# fill-column: 79
# End:
