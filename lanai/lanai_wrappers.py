from __future__ import print_function
import sys
import subprocess as subp
import itertools
import time
import csv
import shutil
import re
from tempfile import NamedTemporaryFile


def mod_file_for_printing(c_fname, def_fname, parallel):
    lines = [line.rstrip('\n') for line in open(c_fname)]
    defs = [line.rstrip('\n') for line in open(def_fname)]

    prnt_vars = []
    # run through the search space spec to get the names of the variables
    # that we want iterated
    for i in range(0, len(defs)):
        if re.match(r'^@iterator', defs[i], re.M):
            i += 1
            mobj = re.match(r'def (.*\()*', defs[i])
            var = str(mobj.group(1))
            var = var[:-1]
            prnt_vars.append(var.rstrip(' '))

        if re.match(r'^(.*)(\s*)=(\s*)range*', defs[i]):
            mobj = re.match(r'^(.*)(\s*)=(\s*)range*', defs[i])
            var = str(mobj.group(1))
            if var[0] != '#':
                prnt_vars.append(var.rstrip(' '))

        if re.match(r"^(.*)(\s*)=(\s*)\[(.*)\]", defs[i]):
            mobj = re.match(r"^(.*)(\s*)=(\s*)\[(.*)\]", defs[i])
            var = str(mobj.group(1))
            if var[0] != '#':
                prnt_vars.append(var.rstrip(' '))


    # construct the header for the csv file and the prepared print statement
    prnt_vars.append("idx")
    var_statement = ""
    for var in prnt_vars:
        var_statement += " " + repr(var).strip("\'") + ","

    var_statement = var_statement[:-1]
    prnt_header = "printf(\"" + var_statement + "\\n\");"

    prnt_statement = "printf(\""
    for var in prnt_vars:
        prnt_statement += " %d,"

    prnt_statement = prnt_statement[:-1]
    prnt_statement += "\\n\"," + var_statement + ");"

    if parallel is True:
        prnt_statement += " }"

    loop_begin = 0
    for i in range(0, len(lines)):
        if re.search(r'iterate\(int idx0\)', lines[i]):
            loop_begin = i

    if loop_begin is 0:
        print(lines)
        raise StandardError("Cannot find iterate function")

    # insert in reverse order at the top of the loop. This is at loop_begin + 2
    if parallel is True:
        lines.insert(loop_begin + 2, "{")
        lines.insert(loop_begin + 2, "#pragma omp parallel")
        omp_pragma2 = "#pragma omp for schedule(dynamic)"

    lines.insert(loop_begin + 2, prnt_header)
    lines.insert(loop_begin + 2, "int volatile idx = idx0;")

    # insert the prepared statements into the .c lines
    for i in range(0, len(lines)):
        if re.search(r'print_placeholder = 1;', lines[i]):
            lines[i] = prnt_statement

        if parallel is True:
            if re.search(r'int omp_pragma;', lines[i]):
                lines[i] = omp_pragma2

    with open(c_fname, 'w+') as f:
        for line in lines:
            f.write(line)
            f.write("\n")

    return


def generate_c_file(args, fname):

    # lanai_path = os.path.dirname(os.path.realpath(__file__))
    print("Creating file %s" % fname)
    gen_statement = "python lanai/lanai.py --generate "
    if args.graphviz:
        gen_statement += "--graphviz " + args.graphviz[0]
    gen_statement += args.iterator[0] + " > " + fname
    rv = subp.call(gen_statement, shell=True)
    if rv != 0:
        raise SyntaxError

    mod_file_for_printing(fname, args.iterator[0], args.omp)
    return


def generate_csv(args, file_base, cfile_name, csvf, cc, parallel):
    working = itertools.cycle(['-', '\\', '|', '/'])
    cc_statement = cc + " "
    cc_statement += "-O3 -Wall -Wno-unused-but-set-variable " + cfile_name + " -o " + file_base + " -lm"
    if parallel is True:
        cc_statement += " -fopenmp"
    print("Executing: " + cc_statement)

    subp.call(cc_statement, shell=True)
    ex_statement = "./" + file_base + " > " + csvf
    print("Executing: " + ex_statement)
    print("Generating space. Please wait.")

    p = subp.Popen(ex_statement, shell=True)
    while p.poll() is None:
        sys.stderr.write(next(working))
        sys.stderr.flush()
        time.sleep(0.5)
        sys.stderr.write('\b')

    if not args.noquiet:
        subp.call("rm " + cfile_name, shell=True)

    # reindex the csv file for 0 indexing
    tempfile = NamedTemporaryFile(mode='wt',delete=False)
    with open(csvf, 'rt') as csvfile, tempfile:
        reader = csv.reader(csvfile, delimiter=',')
        writer = csv.writer(tempfile, delimiter=',')
        row = next(reader)
        writer.writerow(row)

        # necessary for 0 indexing the configurations
        for row in reader:
            row[-1] = int(row[-1]) - 2
            writer.writerow(row)

    shutil.move(tempfile.name, csvf)

    print("Done.")
    return
