#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set fileencoding=utf-8 :

"""
The Python based declarative iterators can be transformed into code by invoking
    this parser:

    python lanai.py --generate file_with_lanai_iterators_and_conditions.py
"""

from __future__ import print_function

import compiler.visitor

import lanaimath
import lanaic_ast
import lanaisymb

math = lanaimath


class AnonymousExpression:
    def __add__(self, other): return IteratorExpression("+", self, other)

    def __div__(self, other): return IteratorExpression("/", self, other)

    def __mul__(self, other): return IteratorExpression("*", self, other)

    def __rmul__(self, other): return IteratorExpression("*", self, other)

    def __rdiv__(self, other): return IteratorExpression("/", other, self)


class NamedExpression(AnonymousExpression):
    def set_parse_name(self, name):
        self.name = name


def get_all_deps(obj):
    l = list()
    if isinstance(obj, (IteratorExpression, FunctionCall)):
        l.extend(obj.get_deps())
    elif isinstance(obj, (Iterator, DecoratedIterator)):
        l.append(obj.name)
    elif isinstance(obj, int):
        pass
    else:
        raise ValueError, obj
    return l


class IteratorExpression(NamedExpression):  # base class for all expressions
    def __init__(self, op, left, right=None):
        self.op = op
        self.left = left
        self.right = right

    def get_deps(self):
        l = list()
        for obj in (self.left, self.right):
            l.extend(get_all_deps(obj))
        return l

    def codegen(self, func, stmt, context):
        func.declare(lanaic_ast.Decl(self.name, lanaic_ast.Int()))
        stmt.append(lanaic_ast.Assign("=", lanaic_ast.Name(self.name),
                                      self.get_c_ast()))
        return stmt

    def get_c_ast(self):
        if self.right is None:
            return lanaic_ast.UnaryOp(self.op, resolve_expr(self.left))
        else:
            return lanaic_ast.BinOp(self.op, resolve_expr(self.left),
                                    resolve_expr(self.right))


class FunctionCall(AnonymousExpression):
    def __init__(self, name, *args):
        self.name = name
        self.args = args

    def get_deps(self):
        l = list()
        for obj in self.args:
            l.extend(get_all_deps(obj))
        return l


class Iterator(NamedExpression):
    """
    range()-like iterator.
    """

    def __init__(self, first, past_last, step):
        self.first = first
        self.past_last = past_last
        self.step = step

    def get_deps(self):
        l = list()
        for obj in self.first, self.past_last, self.step:
            l.extend(get_all_deps(obj))
        return l

    def __repr__(self):
        return "Iterator:" + str(self.name) + str((self.first, self.past_last,
                                                   self.step))

    def codegen(self, func, stmt, context):
        func.declare(lanaic_ast.Decl(self.name, lanaic_ast.Int()))
        loop_body = lanaic_ast.Stmt([])
        stmt.append(lanaic_ast.For(
            lanaic_ast.Assign("=", lanaic_ast.Name(self.name),
                              resolve_expr(self.first)),
            lanaic_ast.BinOp("<", resolve_expr(self.name),
                             resolve_expr(self.past_last)),
            lanaic_ast.Assign("+=", lanaic_ast.Name(self.name),
                              resolve_expr(self.step)), loop_body))
        return loop_body


class IteratorNoDepList(Iterator):
    """
    Iterator defined as a list of values that do not involve other iterators'
    values.
    Hence, the name: no dependence iterator
    """

    def __init__(self, values, name):
        self.values = values
        self.name = name

    def __repr__(self):
        return "IteratorNoDepList:" + str(self.name) + str(self.values)

    def get_deps(self):
        return list()

    def codegen(self, func, stmt, context):
        idx = lanaic_ast.Name(self.name + "_index")
        arr = lanaic_ast.Name(self.name + "_values")
        func.declare(lanaic_ast.Decl(self.name, lanaic_ast.Int()))
        func.declare(lanaic_ast.Decl(idx.name, lanaic_ast.Int()))
        l = list()
        for v in self.values:
            if type(v) is int:
                l.append(lanaic_ast.Const(v))
            else:
                raise TypeError, "Unknown list component %s of type %s" % \
                                 (str(v), str(type(v)))

        func.declare(lanaic_ast.Decl(arr.name, lanaic_ast.Int(),
                                     len(self.values),
                                     lanaic_ast.CompoundInitializer(*l)))

        loop_body = lanaic_ast.Stmt(
            [lanaic_ast.Assign("=", lanaic_ast.Name(self.name),
                               lanaic_ast.IndexOp(arr, idx))])
        stmt.append(lanaic_ast.For(
            lanaic_ast.Assign("=", idx, lanaic_ast.Const(0)),
            lanaic_ast.BinOp("<", idx, lanaic_ast.Const(len(self.values))),
            lanaic_ast.Assign("+=", idx, lanaic_ast.Const(1)), loop_body))

        return loop_body


def resolve_expr(expr):
    if isinstance(expr, str):
        return lanaic_ast.Name(expr)
    elif isinstance(expr, int):
        return lanaic_ast.Const(expr)
    elif isinstance(expr, (Iterator, IteratorFunction)):
        return lanaic_ast.Name(expr.name)
    elif isinstance(expr, IteratorExpression):
        return expr.get_c_ast()
    elif isinstance(expr, FunctionCall):
        return lanaic_ast.CallFunc(expr.name, map(resolve_expr, expr.args))
    else:
        raise ValueError, str(expr)


def min(*args):
    return FunctionCall("min", *args)


def range(*args):
    if 1 == len(args):
        return Iterator(0, args[0], 1)
    elif 2 == len(args):
        return Iterator(args[0], args[1], 1)
    elif 3 == len(args):
        return Iterator(args[0], args[1], args[2])
    else:
        TypeError("range expected at most 3 arguments, got %d" %
                  (len(args) + 1))


class DependenceVisitor:
    def __init__(self):
        self.the_locals = dict()
        self.the_globals = dict()

    def visitAssign(self, node):
        # self.deps.extend(get_deps_expr(node.expr))
        # compiler.visitor.walk(node.expr, self)
        for node1 in node.nodes:
            self.the_locals[node1.name] = 1

    def visitCallFunc(self, node):
        pass

    def visitCompare(self, node):
        pass

    def visitIf(self, node):
        pass

    def visitName(self, node):
        self.the_globals[node.name] = 1

    def default(self, node):  # FIXME: not quite implemented
        raise ValueError, "Not visited " + str(node.__class__)


class EmptyLexicalContext:
    def __init__(self):
        self.the_globals = dict()
        self.the_locals = dict()


class DependenceWalker(EmptyLexicalContext):
    def get_deps(self, node):
        self._walk(node)
        l = list()
        for k in self.the_globals:
            if not self.the_locals.has_key(k):
                l.append(k)
        return l

    def _walk(self, node):
        for node1 in node.getChildNodes():
            if isinstance(node1, compiler.ast.Name):
                self.the_globals[node1.name] = 1
            elif isinstance(node1, compiler.ast.AssName):
                self.the_locals[node1.name] = 1
            else:
                self._walk(node1)


class DecoratedIterator(NamedExpression):
    def __init__(self, func, func_ast):
        self.func = func
        self.func_ast = func_ast
        self.dependences = None

    def get_deps_failed(self):
        if self.dependences is None:
            vis = DependenceVisitor()
            compiler.visitor.walk(self.func_ast, vis)
            l = list()
            for k in vis.the_globals:
                if not vis.the_locals.has_key(k):
                    l.append(k)
            self.dependences = l
        return self.dependences[:]

    def get_deps(self):
        return DependenceWalker().get_deps(self.func_ast)


class IteratorFunction(DecoratedIterator):
    def __add__(self, other):
        return IteratorExpression("+", self, other)

    def codegen(self, func, stmt, context):
        func.declare(lanaic_ast.Decl(self.name, lanaic_ast.Int()))
        return lanaisymb.parse(self.func_ast, func, stmt, context)


class ConditionFunction(DecoratedIterator):
    def codegen(self, func, stmt, context):
        return lanaisymb.parse(self.func_ast, func, stmt, context, True)


def iterator(func, func_ast=None):
    # print "ITERATOR", repr(func), dir(func), func.func_code
    return IteratorFunction(func, func_ast)


def condition(func, func_ast=None):
    return ConditionFunction(func, func_ast)


if "__main__" == __name__:
    import sys

    if "--generate" in sys.argv[1:]:
        import lanaic_gen

        sys.exit(lanaic_gen.main(sys.argv))

    sys.exit(0)

# Local Variables:
# mode: python
# tab-width: 4
# indent-tabs-mode: nil
# fill-column: 79
# End:
