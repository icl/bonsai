#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set fileencoding=utf-8 :

from __future__ import print_function

import compiler.ast

import lanai
import lanaic_ast


class SymbolicContext:
    """
    There is one SymbolicContext for every iterator defined as function.
    """

    def __init__(self, node, func, stmt, context, condition_function):
        self.prefix = node.name
        self.func = func
        self.stmt = stmt
        self.context = context
        self.condition_function = condition_function
        if not condition_function:  # only iterators need looping variables
            self.var_start = lanaic_ast.Name(self.declare_local("start"))
            self.var_bound = lanaic_ast.Name(self.declare_local("bound"))
            self.var_delta = lanaic_ast.Name(self.declare_local("delta"))

    def declare_local(self, name):
        mangled_name = self.prefix + "_" + name
        self.func.declare(lanaic_ast.Decl(mangled_name, lanaic_ast.Int()))
        return mangled_name

    def parse_and(self, node):
        if len(node.nodes) == 2:
            return lanaic_ast.BinOp("&&",
                                    self.parse_expr(node.nodes[0]),
                                    self.parse_expr(node.nodes[1]))
        raise ValueError

    def parse_expr(self, node):
        full_class_name = str(node.__class__)
        comp_ast_mod = "compiler.ast."
        if full_class_name.startswith(comp_ast_mod):
            return getattr(self, "parse_" +
                           full_class_name[len(comp_ast_mod):].lower())(node)
        else:
            raise ValueError, str(node)

    def parse_add(self, node):
        return lanaic_ast.BinOp("+", self.parse_expr(node.left),
                                self.parse_expr(node.right))

    def parse_assign(self, node, stmt):
        expr = self.parse_expr(node.expr)
        for node1 in node.nodes:
            if isinstance(node1, compiler.ast.AssName) and \
                    node1.flags == "OP_ASSIGN":
                # a known value
                if self.context.the_globals.has_key(node1.name):
                    stmt.append(lanaic_ast.Assign("=",
                                                  lanaic_ast.Name(node1.name),
                                                  expr))
                else:  # a local value FIXME: might need scrambling
                    stmt.append(
                        lanaic_ast.Assign("=",
                                          lanaic_ast.Name(
                                              self.declare_local(node1.name)),
                                          expr))

    def parse_callfunc(self, node):
        assert node.star_args is None and node.dstar_args is None
        func = self.parse_expr(node.node)
        args = [self.parse_expr(arg) for arg in node.args]
        return lanaic_ast.CallFunc(func, args)

    def parse_compare(self, node):
        expr0 = self.parse_expr(node.expr)
        if len(node.ops) == 1:
            op, expr_ast = node.ops[0]
            return lanaic_ast.BinOp(op, expr0, self.parse_expr(expr_ast))
        else:
            raise ValueError
            for op, expr_ast in node.ops:
                append(BinOp(op, expr0))

    def parse_const(self, node):
        return lanaic_ast.Const(node.value)

    def parse_div(self, node):
        return lanaic_ast.BinOp("/",
                                self.parse_expr(node.left),
                                self.parse_expr(node.right))

    def parse_getattr(self, node):
        if node.expr.name == "math":
            return lanaic_ast.Name(node.attrname)

    def parse_function(self, node, stmt):
        self.parse_stmt(node.code, stmt)

    def parse_if(self, node, stmt0):
        tests = list()
        for expr, stmt in node.tests:
            e = self.parse_expr(expr)
            if e is None:
                print(expr, stmt)
            tstmt = lanaic_ast.Stmt([])
            self.parse_stmt(stmt, tstmt)
            tests.append((e, tstmt))
        if node.else_ is None:
            else_ = None
        else:
            else_ = lanaic_ast.Stmt([])
            self.parse_stmt(node.else_, else_)
        stmt1 = lanaic_ast.If(tests, else_)
        stmt0.append(stmt1)

    def parse_mod(self, node):
        return lanaic_ast.BinOp("%", self.parse_expr(node.left),
                                self.parse_expr(node.right))

    def parse_mul(self, node):
        return lanaic_ast.BinOp("*", self.parse_expr(node.left),
                                self.parse_expr(node.right))

    def parse_name(self, node):
        # this is a global variable
        if self.context.the_globals.has_key(node.name):
            obj = self.context.the_globals[node.name]
            # if it is one of the iterators/conditions
            if isinstance(obj, lanai.NamedExpression):
                pass
            else:  # it must be a Python global, try to convert to C
                if isinstance(obj, int):
                    self.func.declare(lanaic_ast.Decl(node.name,
                                                      lanaic_ast.Int(),
                                                      None,
                                                      lanaic_ast.Const(obj)))
                else:
                    raise ValueError, "%s:%s" % (repr(type(obj)), repr(obj))
            return lanaic_ast.Name(node.name)

        else:
            mangled_name = self.declare_local(node.name)
            return lanaic_ast.Name(mangled_name)

    def parse_or(self, node):
        if len(node.nodes) == 2:
            return lanaic_ast.BinOp("||", self.parse_expr(node.nodes[0]),
                                    self.parse_expr(node.nodes[1]))
        raise ValueError

    def parse_return(self, node, stmt):
        if self.condition_function:
            stmt.append(lanaic_ast.If([(self.parse_expr(node.value),
                                        lanaic_ast.Stmt(
                                            [lanaic_ast.Continue()])
                                        )], None))
        else:
            if isinstance(node.value, compiler.ast.CallFunc) and \
                    isinstance(node.value.node, compiler.ast.Name) and \
                    node.value.node.name == "range":

                if len(node.value.args) == 3:
                    start = self.parse_expr(node.value.args[0])
                    bound = self.parse_expr(node.value.args[1])
                    delta = self.parse_expr(node.value.args[2])
                elif len(node.value.args) == 2:
                    start = self.parse_expr(node.value.args[0])
                    bound = self.parse_expr(node.value.args[1])
                    delta = lanaic_ast.Const(1)
                elif len(node.value.args) == 1:
                    start = lanaic_ast.Const(0)
                    bound = self.parse_expr(node.value.args[0])
                    delta = lanaic_ast.Const(1)
                else:
                    raise ValueError, repr(args)
                stmt.append(lanaic_ast.Assign("=", self.var_start, start))
                stmt.append(lanaic_ast.Assign("=", self.var_bound, bound))
                stmt.append(lanaic_ast.Assign("=", self.var_delta, delta))
                stmt.append(lanaic_ast.Break())
            else:
                raise ValueError, str(node)

    def parse_stmt(self, node, stmt):
        comp_ast_mod = "compiler.ast."
        for node1 in node.nodes:
            full_class_name = str(node1.__class__)
            if full_class_name.startswith(comp_ast_mod):
                getattr(self,
                        "parse_" +
                        full_class_name[len(comp_ast_mod):].lower()
                        )(node1, stmt)
            else:
                raise ValueError, "Unknown class " + full_class_name

    def parse_unarysub(self, node):
        expr = self.parse_expr(node.expr)
        if isinstance(expr, lanaic_ast.Const):
            expr.value = -expr.value
            return expr
        return lanaic_ast.UnaryOp("-", self.parse_expr(node.expr))

    def make_loop(self, enclosing_stmt):
        loop_body = lanaic_ast.Stmt([])

        var = lanaic_ast.Name(self.prefix)

        zero = lanaic_ast.Const(0)

        # use generic for() until it's possible to optimize it
        for_ = lanaic_ast.For(
            lanaic_ast.Assign("=", var, self.var_start),
            lanaic_ast.BinOp("<", var, self.var_bound),
            lanaic_ast.Assign("+=", var, self.var_delta), loop_body)

        if isinstance(self.var_delta, lanaic_ast.Const):
            if self.var_delta.value == 0:  # delta is 0, skip it
                print("Step for iterator is 0.")
                return enclosing_stmt  # no loop
            else:
                if self.var_delta.value < 0:
                    for_.condition = lanaic_ast.BinOp(">", var, self.var_bound)
                    for_.increment = \
                        lanaic_ast.Assign("-=",
                                          var,
                                          lanaic_ast.Const(
                                              -self.var_delta.value))

                # no if() statement to check for delta!=0
                enclosing_stmt.append(for_)

        else:  # delta is an expression
            # check at runtime if 'delta' is 0
            if_ = lanaic_ast.If([(lanaic_ast.BinOp("!=",
                                                   strip_unary_op(
                                                       self.var_delta),
                                                   zero), for_)], None)
            for_.condition = lanaic_ast.TernaryOp(
                lanaic_ast.BinOp(">", self.var_delta, zero),
                lanaic_ast.BinOp("<", var, self.var_bound),
                lanaic_ast.BinOp(">", var, self.var_bound))
            enclosing_stmt.append(if_)

        return loop_body


def strip_unary_op(node):
    if isinstance(node, lanaic_ast.UnaryOp):
        return node.expr
    return node


def parse(node, func, stmt, context, condition_function=False):
    sctxt = SymbolicContext(node, func, stmt, context, condition_function)
    assert isinstance(node, compiler.ast.Function)  # only parsing functions
    if stmt is None:
        print("NONE", node)
    if condition_function:
        sctxt.parse_function(node, stmt)
        return stmt
    else:
        body = lanaic_ast.Stmt([])
        stmt.append(lanaic_ast.DoWhile(lanaic_ast.Const(0), body))
        sctxt.parse_function(node, body)
        # must return a loop body to insert new iterators into it
        return sctxt.make_loop(stmt)

# Local Variables:
# mode: python
# tab-width: 4
# indent-tabs-mode: nil
# fill-column: 79
# End:
