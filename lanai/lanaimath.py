#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set fileencoding=utf-8 :

import math

import lanai


def floor(x):
    if type(x) == float:
        return math.floor(x)
    else:
        return lanai.IteratorExpression("floor", x)


# Local Variables:
# mode: python
# tab-width: 4
# indent-tabs-mode: nil
# fill-column: 79
# End:
