//------------------------------------------------------------------------------
// Copyright (c) 2018, University of Tennessee
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the University of Tennessee nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL UNIVERSITY OF TENNESSEE BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//------------------------------------------------------------------------------
// This material is based upon work supported by the National Science Foundation
// under Grant No. 1642441.
//------------------------------------------------------------------------------
// For assistance with BONSAI, email <bonsai-user@icl.utk.edu>.
//------------------------------------------------------------------------------

#ifndef BONSAI_DEVICE_HH
#define BONSAI_DEVICE_HH

#include "bonsai_Exception.hh"

#include <cuda.h>

namespace bonsai {

//------------------------------------------------------------------------------
/// \class
/// \brief
///     Represenda a device / accelerator / GPU.
///     Facilitates reservation for exclusive access.
///     Provides a unique stream for execution on the device.
///
class Device {
public:
    ~Device()
    {
        cudaSetDevice(num_);
        cudaStreamDestroy(stream_);
    }

    void init(int num)
    {
        num_ = num;
        cuda_call(cudaSetDevice(num_));
        cuda_call(cudaStreamCreate(&stream_));
        free_ = true;
    }

    void set() const
    {
        cuda_call(cudaSetDevice(num_));
    }

    cudaStream_t stream() const
    {
        return stream_;
    }

    /// \brief
    ///     Attempts to reserve the device for exclusive access.
    /// \returns
    ///     `true` if the device was successfully reserved.
    bool reserve()
    {
        // If free_ == true, swap to false and return true.
        return __sync_bool_compare_and_swap(&free_, true, false);
    }

    /// \brief
    ///     Releases the device from exclusive access.
    void release()
    {
        // Set free_ to true. Confirm that the old value was false.
        assert(__sync_lock_test_and_set(&free_, true) == false);
    }

private:
    int num_;             ///< the device's number
    bool free_;           ///< Indicates if the device is free.
    cudaStream_t stream_; ///< a unique stream associated with the device
};

} // namespace bonsai

#endif // BONSAI_DEVICE_HH
