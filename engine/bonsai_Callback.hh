//------------------------------------------------------------------------------
// Copyright (c) 2018, University of Tennessee
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the University of Tennessee nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL UNIVERSITY OF TENNESSEE BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//------------------------------------------------------------------------------
// This material is based upon work supported by the National Science Foundation
// under Grant No. 1642441.
//------------------------------------------------------------------------------
// For assistance with BONSAI, email <bonsai-user@icl.utk.edu>.
//------------------------------------------------------------------------------

#ifndef BONSAI_CALLBACK_HH
#define BONSAI_CALLBACK_HH

#include "bonsai_Exception.hh"
#include "bonsai_Kernel.hh"
#include "bonsai_Trace.hh"
#include "bonsai_types.hh"

#include <map>
#include <unistd.h>

namespace bonsai {

//------------------------------------------------------------------------------
/// \class
/// \brief
///     Represents a user-supplied callback function. Each run in the autotuning
///     sweep is implemented by a sequence of user-supplied callbacks.
///
class Callback {
public:
    typedef bool (*HostFunc  )(void*, KernelParams&);
    typedef bool (*DevFunc   )(void*, KernelParams&, cudaStream_t);
    typedef bool (*KernelFunc)(void*, KernelParams&, cudaStream_t, Kernel::Func);

    union Func {
    public:
        Func()
        {}

        Func (HostFunc func)
            : host_(func)
        {}

        Func (DevFunc func)
            : device_(func)
        {}

        Func (KernelFunc func)
            : kernel_(func)
        {}

        HostFunc host_;     ///< host callback function pointer
        DevFunc device_;    ///< device callback function pointer
        KernelFunc kernel_; ///< kernel (device) callback function pointer
    };

    enum class Target {
        Host,  ///< executed by a host thread concurrently with other tasks
        Device ///< executed by one of the devices with exclusive access
    };

    enum class Type {
        Kernel,  ///< executed on a device with multiple repetitions
        Test,    ///< returns the outcome of a user-supplied test
        Cleanup, ///< only executed once, at the end of the sequence
        Other
    };

    enum class Marker {
        None           = 0b0000,
        StartCycle     = 0b0001, ///< Cycle starts before this callback.
        StopCycle      = 0b0010, ///< Cycle stops after this callback.
        StartTimer     = 0b0100, ///< Timer starts before this callback.
        StopTimer      = 0b1000, ///< Timer stops after this callback.
        StartStopCycle = StartCycle | StopCycle, ///< Only repeat this callback.
        StartStopTimer = StartTimer | StopTimer  ///< Only time this callback.
    };

    friend Marker operator |(Callback::Marker lhs, Callback::Marker rhs);
    friend Marker operator &(Callback::Marker lhs, Callback::Marker rhs);

    Callback()
    {}

    /// \brief
    ///     Constructs a host callback.
    Callback(HostFunc func, Target target, Type type, Marker marker)
        : func_(func),
          target_(target),
          type_(type),
          marker_(marker),
          last_device_(false)
    {
        assert(target == Target::Host);
        func_.host_ = func;
    }

    /// \brief
    ///     Constructs a device callback.
    Callback(DevFunc func, Target target, Type type, Marker marker)
        : func_(func),
          target_(target),
          type_(type),
          marker_(marker),
          last_device_(false)
    {
        assert(target == Target::Device);
    }

    /// \brief
    ///     Constructs a kernel (device) callback.
    Callback(KernelFunc func, Target target, Type type, Marker marker)
        : func_(func),
          target_(target),
          type_(type),
          marker_(marker),
          last_device_(false)
    {
        assert(target == Target::Device);
        assert(type == Type::Kernel);
    }

    /// \brief
    ///     Calls a host callback.
    bool call(void* callback_data,
              KernelParams& kernel_params) const
    {
        Trace::Block trace_block("host callback");
        assert(target_ == Target::Host);
        return func_.host_(callback_data, kernel_params);
    }

    /// \brief
    ///     Calls a device callback.
    bool call(void* callback_data,
              KernelParams& kernel_params,
              cudaStream_t stream) const
    {
        Trace::Block trace_block("device callback");
        assert(target_ == Target::Device);
        bool retval = func_.device_(callback_data, kernel_params, stream);
        cudaStreamSynchronize(stream);
        return retval;
    }

    /// \brief
    ///     Calls a kernel (device) callback.
    bool call(void* callback_data,
              KernelParams& kernel_params,
              cudaStream_t stream,
              Kernel::Func kernel_func) const
    {
        Trace::Block trace_block("kernel launch");
        assert(target_ == Target::Device);
        assert(type_ == Type::Kernel);
        bool retval = func_.kernel_(callback_data, kernel_params,
                                    stream, kernel_func);
        cudaStreamSynchronize(stream);
        return retval;
    }

    // target_ accessors
    bool isHost() const
    {
        return (target_ == Target::Host);
    }

    bool isDevice() const
    {
        return (target_ == Target::Device);
    }

    // type_ accessors
    bool isKernel() const
    {
        return (type_ == Type::Kernel);
    }

    bool isTest() const
    {
        return (type_ == Type::Test);
    }

    bool isCleanup() const
    {
        return (type_ == Type::Cleanup);
    }

    // marker_ accessors (cycle)
    bool isStartCycle() const
    {
        return ((marker_ & Marker::StartCycle) == Marker::StartCycle);
    }

    bool isStopCycle() const
    {
        return ((marker_ & Marker::StopCycle) == Marker::StopCycle);
    }

    void setStartCycle()
    {
        marker_ = marker_ | Marker::StartCycle;
    }

    void setStopCycle()
    {
        marker_ = marker_ | Marker::StopCycle;
    }

    void setStartStopCycle()
    {
        marker_ = marker_ | Marker::StartCycle | Marker::StopCycle;
    }

    // marker_ accessors (timer)
    bool isStartTimer() const
    {
        return ((marker_ & Marker::StartTimer) == Marker::StartTimer);
    }

    bool isStopTimer() const
    {
        return ((marker_ & Marker::StopTimer) == Marker::StopTimer);
    }

    void setStartTimer()
    {
        marker_ = marker_ | Marker::StartTimer;
    }

    void setStopTimer()
    {
        marker_ = marker_ | Marker::StopTimer;
    }

    void setStartStopTimer()
    {
        marker_ = marker_ | Marker::StartTimer | Marker::StopTimer;
    }

    // last_device_ accessors
    void setLastDevice(bool val)
    {
        last_device_ = val;
    }

    bool isLastDevice() const
    {
        return (last_device_ == true);
    }

private:
    Func func_;        ///< callback function pointer
    Target target_;    ///< execution target (Host|Device)
    Type type_;        ///< callback type (Kernel|Test|Cleanup|Other)
    Marker marker_;    ///< callback marker (StartCycle|StopCycle|...)
    bool last_device_; ///< last device callback
};

// http://blog.bitwigglers.org/using-enum-classes-as-type-safe-bitmasks/
Callback::Marker operator |(Callback::Marker lhs, Callback::Marker rhs)
{
    return static_cast<Callback::Marker> (
        static_cast<std::underlying_type<Callback::Marker>::type>(lhs) |
        static_cast<std::underlying_type<Callback::Marker>::type>(rhs)
    );
}

Callback::Marker operator &(Callback::Marker lhs, Callback::Marker rhs)
{
    return static_cast<Callback::Marker> (
        static_cast<std::underlying_type<Callback::Marker>::type>(lhs) &
        static_cast<std::underlying_type<Callback::Marker>::type>(rhs)
    );
}

} // namespace bonsai

#endif // BONSAI_CALLBACK_HH
