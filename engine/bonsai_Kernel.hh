//------------------------------------------------------------------------------
// Copyright (c) 2018, University of Tennessee
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the University of Tennessee nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL UNIVERSITY OF TENNESSEE BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//------------------------------------------------------------------------------
// This material is based upon work supported by the National Science Foundation
// under Grant No. 1642441.
//------------------------------------------------------------------------------
// For assistance with BONSAI, email <bonsai-user@icl.utk.edu>.
//------------------------------------------------------------------------------

#ifndef BONSAI_KERNEL_HH
#define BONSAI_KERNEL_HH

#include "bonsai_Exception.hh"
#include "bonsai_Trace.hh"

#include <cstdlib>
#include <dlfcn.h>
#include <iostream>
#include <regex>
#include <string>

namespace bonsai {

//------------------------------------------------------------------------------
/// \class
/// \brief
///     Represents a kernel with a particular set of tuning parameters.
///     Implements compilation, launching, and removal of the kernel.
///
class Kernel {
public:
    typedef cudaError_t (*Func)(void*, cudaStream_t);

    Kernel(const std::string& src_filename,
           const std::string& param_string,
           const std::string& func_name,
           const std::string& scratch_path,
           int idx)
        : src_filename_(src_filename),
          param_string_(param_string),
          func_name_(func_name),
          scratch_path_(scratch_path),
          idx_(idx)
    {
        // kernel.cu -> kernel_1.so, kernel_2.so, etc.
        std::regex pattern("([^\\.]+)\\.([^\\.]+)");
        lib_filename_ = std::regex_replace(src_filename_, pattern, "$1");
        lib_filename_ += "_" + std::to_string(idx_) + ".so";
    }

    Func func() const
    {
        return func_;
    }

    /// \brief
    ///     Compiles the kernel to a dynamic library.
    ///     Puts the kernel's number in the filename.
    ///     Appends the parameters' string to the compile line.
    ///     Does all the work in the local scratch.
    bool compile(bool verbose = false) const
    {
        Trace::Block trace_block("kernel compilation");
        std::string cmd_str("nvcc "
                            "--compiler-options '-fPIC' "
                            "--shared -o ");
        cmd_str += scratch_path_ + lib_filename_ + " ";
        cmd_str += scratch_path_ + src_filename_ + " ";
        cmd_str += param_string_ + " ";

        if (!verbose)
            cmd_str += ">/dev/null 2>&1";
        else
            std::cout << cmd_str << std::endl;

        int retval = std::system(cmd_str.c_str());
        return (retval == 0 ? true : false);
    }

    /// \brief
    ///     Opens the dynamic library containing the kernel.
    void open()
    {
        handle_ = dlopen((scratch_path_ + lib_filename_).c_str(), RTLD_NOW);
        assert(handle_ != nullptr);
    }

    /// \brief
    ///     Loads the kernel's function from the dynamic library.
    void load()
    {
        func_ = (Func)dlsym(handle_, func_name_.c_str());
        assert(func_ != nullptr);
    }

    /// \brief
    ///     Closes the dynamic library.
    void close() const
    {
        int retval = dlclose(handle_);
        assert(retval == 0);
    }

    /// \brief
    ///     Deletes the kernel's dynamic library from the filesystem.
    void remove() const
    {
        std::string fullpath(scratch_path_ + lib_filename_);
        int retval = std::remove(fullpath.c_str());
        assert(retval == 0);
    }

private:
    const std::string src_filename_; ///< kernel's source filename
    const std::string param_string_; ///< parameters' string for compilation
    const std::string func_name_;    ///< kernel's function name
    const std::string scratch_path_; ///< path to the local scratch directory
    std::string lib_filename_;       ///< kernel's dynamic library filename
    void* handle_;                   ///< kernel's dynamic library handle
    Func func_;                      ///< kernel's function pointer
    int idx_;                        ///< kernel's number
};

} // namespace bonsai

#endif // BONSAI_KERNEL_HH
