//------------------------------------------------------------------------------
// Copyright (c) 2018, University of Tennessee
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the University of Tennessee nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL UNIVERSITY OF TENNESSEE BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//------------------------------------------------------------------------------
// This material is based upon work supported by the National Science Foundation
// under Grant No. 1642441.
//------------------------------------------------------------------------------
// For assistance with BONSAI, email <bonsai-user@icl.utk.edu>.
//------------------------------------------------------------------------------

#ifndef BONSAI_TYPES_HH
#define BONSAI_TYPES_HH

#include <map>
#include <string>

namespace bonsai {

//------------------------------------------------------------------------------
/// \class
/// \brief
///     Represents a value of a parameter, which can be of different types.
///
typedef union Value {
    ///< types of parameters
    enum class Type {
        Real,    ///< real number represented as double
        Integer, ///< integer number represented as int64_t
        String   ///< string represented as char[MaxStringSize]
    };

    ///< maximum size of string parameter
    static const std::size_t MaxStringSize = 8;

    // just a precaution
    // should be zeroed by default
    // will cause inconspicuous errors if not
    Value() { memset(this, 0, sizeof(Value)); }

    /// \brief
    ///     Prints the value according to the type.
    ///     Templated for use with ostream and ofstream.
    template <typename T>
    void print(T& output, Type type) const
    {
        switch (type) {
            case Type::Real:
                output << this->real;
                break;
            case Type::Integer:
                output << this->integer;
                break;
            case Type::String:
                output << std::string(this->string, MaxStringSize).c_str();
                break;
        }
    }

    /// \brief
    ///     Returns the name string for the type.
    static std::string typeName(Type type)
    {
        switch (type) {
            case Type::Real: return std::string("Real");
            case Type::Integer: return std::string("Integer");
            case Type::String: return std::string("String");
            default: assert(0);
        }
    }

    /// \brief
    ///     Converts the character array to a string.
    ///     Removes the first null character and everything that follows.
    std::string toString() const
    {
        std::string str(string, MaxStringSize);
        std::size_t pos = str.find_first_of('\0');
        if (pos != std::string::npos)
            str.resize(pos);
        return str;
    }

    double real;                ///< floating-point parameter
    int64_t integer;            ///< (signed) integer parameter
    char string[MaxStringSize]; ///< (fixed size) string parameter
} Value;

//------------------------------------------------------------------------------
/// \namespace
/// \brief
///     Distinguishes between compile-time and runtime parameters.
///
namespace Param {
    ///< kinds of parameters
    enum class Kind {
        Compile, ///< compile-time input parameter
        Runtime, ///< runtime input parameter
        Output   ///< output parameter
    };

    /// \brief
    ///     Returns the name string.
    std::string kindName(Kind kind)
    {
        switch (kind) {
            case Kind::Compile: return std::string("Compile");
            case Kind::Runtime: return std::string("Runtime");
            case Kind::Output: return std::string("Output");
            default: assert(0);
        }
    }
}

/// a key-value map of parameters for one row of the sweep
typedef std::map<std::string, Value> KernelParams;

} // namespace bonsai

#endif // BONSAI_TYPES_HH
