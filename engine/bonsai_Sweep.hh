//------------------------------------------------------------------------------
// Copyright (c) 2018, University of Tennessee
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the University of Tennessee nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL UNIVERSITY OF TENNESSEE BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//------------------------------------------------------------------------------
// This material is based upon work supported by the National Science Foundation
// under Grant No. 1642441.
//------------------------------------------------------------------------------
// For assistance with BONSAI, email <bonsai-user@icl.utk.edu>.
//------------------------------------------------------------------------------

#ifndef BONSAI_SWEEP_HH
#define BONSAI_SWEEP_HH

#include "bonsai_Callback.hh"
#include "bonsai_Device.hh"
#include "bonsai_Exception.hh"
#include "bonsai_Kernel.hh"
#include "bonsai_Trace.hh"
#include "bonsai_types.hh"

#include <cassert>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <unistd.h>
#include <vector>

#include <cuda_runtime.h>
#include <mpi.h>
#include <omp.h>

namespace bonsai {

//------------------------------------------------------------------------------
/// \class
/// \brief
///     Implements a complete autotuning sweep.
///     Spreads the work across multiple nodes.
///     Uses multiple threads for compilation.
///     Uses multiple devices for timing runs.
///
class Sweep {
public:
    Sweep(const std::string& input_filename,
          const std::string& output_filename,
          const std::string& kernel_filename,
          const std::string& kernel_func_name,
          const std::size_t callback_data_size,
          int num_reps,
          int chunk_size,
          const std::string& compile_flags = {},
          const std::string& scratch_path = {});
    ~Sweep();

    template <typename Func>
    void addCallback(Func func,
                     Callback::Target target,
                     Callback::Type type = Callback::Type::Other,
                     Callback::Marker marker = Callback::Marker::None);

    template <typename Func>
    void addCallback(Func func,
                     Callback::Target target,
                     Callback::Marker marker);

    void addParameter(std::string param_name, Value::Type param_type);
    void run();

private:
    void readInput();
    void bcastInput();

    KernelParams getParamMap(int idx) const;
    std::string getCompileString(int idx) const;
    void updateParams(const KernelParams& kernel_params, int idx);
    void printParams() const;

    void requestChunk() const;
    int64_t receiveChunk() const;
    void processChunk(int64_t chunk_start);
    int recvChunkRequest() const;
    void sendChunkResponse(int64_t chunk_start, int requester) const;

    void findCycle();
    void findTimer();
    void collectCycleCallbacks(std::list<Callback>& cycle_callbacks) const;
    void moveCleanupCallbacks(std::list<Callback>& cleanup_callbacks);
    void markLastDeviceCallback();
    void assemblePipeline();

    int reserveDevice();
    void runRow(std::size_t idx);
    void printProgress(int chunk_start) const;

    void gatherOutput();
    void writeOutput() const;

    MPI_Comm mpi_comm_;      ///< MPI communicator
    int mpi_size_;           ///< MPI size
    int mpi_rank_;           ///< local MPI rank
    const int mpi_root_ = 0; ///< MPI root process
    const int mpi_tag_ = 0;  ///< tag for all MPI calls

    int num_devices_;             ///< number of devices
    std::vector<Device> devices_; ///< vector of Device objects

    const std::string input_filename_;   ///< input CSV filename
    const std::string output_filename_;  ///< output CSV filename
    const std::string kernel_filename_;  ///< kernel's source filename
    const std::string kernel_func_name_; ///< kernel's function name

    std::vector<std::string> param_names_;       ///< names of all parameters
    std::vector<Value::Type> param_types_;       ///< types of all parameters
    std::vector<Param::Kind> param_kinds_;       ///< kinds of all parameters
    std::set<std::string> in_param_names_;       ///< names of in parameters
    std::vector<std::vector<Value>> param_vals_; ///< 2D array of values
    int64_t num_rows_;                           ///< number of rows

    const std::size_t callback_data_size_; ///< size of the callback data block
    std::list<Callback> callbacks_;        ///< list of user-supplied callbacks

    int num_reps_;              ///< how many reps per one kernel configuration
    int chunk_size_;            ///< how many kernels sent to one node at a time
    std::string compile_flags_; ///< user's extra compile flags
    std::string scratch_path_;  ///< path to the local scratch directory
    bool verbose_;              ///< verbose mode
};

//------------------------------------------------------------------------------
/// \brief
///     Constructs the Sweep object.
///     Initializes MPI.
///     Initializes devices.
///     Reads and broadcasts the data from the input CSV file.
///
/// \param[in] input_filename
///     name of the input CSV file
///
/// \param[in] output_filename
///     name of the output CSV filename
///
/// \param[in] kernel_filename
///     name of the kernel's source file
///
/// \param[in] kernel_func_name
///     name of the kernel's function
///
/// \param[in] callback_data_size
///     size of the kernel's data block
///
/// \param[in] num_reps
///     how many reps per one kernel configuration
///
/// \param[in] chunk_size
///     how many kernels sent to one node at a time
///
/// \param[in] compile_flags
///     user's extra compile flags (same across the entire sweep)
///
/// \param[in] scratch_path
///     path to the local scratch directory,
///     if empty, set to the value of `$TMPDIR`,
///     if `$TMPDIR` empty, set to `/tmp/`
///
Sweep::Sweep(const std::string& input_filename,
             const std::string& output_filename,
             const std::string& kernel_filename,
             const std::string& kernel_func_name,
             const std::size_t callback_data_size,
             int num_reps,
             int chunk_size,
             const std::string& compile_flags,
             const std::string& scratch_path)
    : mpi_comm_(MPI_COMM_WORLD),
      input_filename_(input_filename),
      output_filename_(output_filename),
      kernel_filename_(kernel_filename),
      kernel_func_name_(kernel_func_name),
      callback_data_size_(callback_data_size),
      num_reps_(num_reps),
      chunk_size_(chunk_size),
      compile_flags_(compile_flags),
      scratch_path_(scratch_path)
{
    // Initialize MPI.
    int required = MPI_THREAD_SERIALIZED;
    int provided;
    mpi_call(MPI_Init_thread(nullptr, nullptr,  required, &provided));
    assert(provided == MPI_THREAD_SERIALIZED);

    mpi_call(MPI_Comm_rank(mpi_comm_, &mpi_rank_));
    mpi_call(MPI_Comm_size(mpi_comm_, &mpi_size_));

    // Initialize devices.
    cuda_call(cudaGetDeviceCount(&num_devices_));
    devices_.resize(num_devices_);
    for (int dev = 0; dev < num_devices_; ++dev)
        devices_[dev].init(dev);

    // Barrier to sync the trace.
    {
        Trace::Block trace_block("MPI_Barrier");
        mpi_call(MPI_Barrier(mpi_comm_));
    }

    // Read the sweep data.
    if (mpi_rank_ == mpi_root_)
        readInput();

    // Broadcast the sweep data.
    bcastInput();

    // Add output parameters: Status, Error, and Time.
    addParameter(std::string("Status"), Value::Type::String);
    addParameter(std::string("Error"), Value::Type::String);
    addParameter(std::string("Time"), Value::Type::Real);

    // If scratch path empty, set it to the value of $TMPDIR.
    // If $TMPDIR empty, set scratch path to "/tmp/".
    if (scratch_path_.empty()) {
        char* tmpdir = std::getenv("TMPDIR");
        if (tmpdir != nullptr)
            scratch_path_ = std::string(tmpdir);
        else
            scratch_path_ = std::string("/tmp/");
    }
    // If no '/' at the end, append it.
    if (scratch_path_.back() != '/')
        scratch_path_.append(1, '/');

    // Copy the kernel source to the local scratch.
    std::string cmd_str("cp -f ");
    cmd_str += kernel_filename_ + " ";
    cmd_str += scratch_path_ + kernel_filename_;
    int retval = std::system(cmd_str.c_str());
    assert(retval == 0);

    // If BONSAI_VERBOSE not empty, set verbose mode.
    char* verbose = std::getenv("BONSAI_VERBOSE");
    if (verbose != nullptr)
        verbose_ = true;
}

//------------------------------------------------------------------------------
/// \brief
///     Destroys the Sweep object.
///     Gathers results from all nodes.
///     Writes the CSV file with the output data.
///     Writes the trace.
///     Shuts down MPI.
///
Sweep::~Sweep()
{
    gatherOutput();
    if (mpi_rank_ == mpi_root_)
        writeOutput();

    // Remove the kernel source file from the local scratch.
    std::string cmd_str("rm -f " + scratch_path_ + kernel_filename_);
    int retval = std::system(cmd_str.c_str());
    // Sometimes fails for unknown reasons.
    // Therefore suppressing the error check.
    // assert(retval == 0);

    // An alternative way to remove the file.
    // But no way to ignore if does not exist.
    // int retval = remove(cmd_str.c_str());
    // assert(retval == 0);

    Trace::finish();
    MPI_Finalize();
}

//------------------------------------------------------------------------------
/// \brief
///     Adds a callback to the list.
///     Allows to set the type and the marker.
///
/// \tparam Func
///     callback function type
///     - HostFunc - host function
///     - DevFunc  - device function
///
/// \param[in] func
///     callback function pointer
///
/// \param[in] target
///     callback execution target:
///     - Host   - executed by a host thread concurrently with other tasks
///     - Device - executed by one of the devices with exclusive access
///
/// \param[in] type
///     callback type:
///     - Kernel  - executed on a device with multiple repetitions
///     - Test    - returns the outcome of a user-supplied test
///     - Cleanup - only executed once, at the end of the sequence
///     - Other   - no special meaning (default)
///
/// \param[in] marker
///     callback marker:
///     - None           - no markers (default)
///     - StartCycle     - Cycle starts before this callback.
///     - StopCycle      - Cycle stops after this callback.
///     - StartTimer     - Timer starts before this callback.
///     - StopTimer      - Timer stops after this callback.
///     - StartStopCycle - Only repeat this callback.
///     - StartStopTimer - Only time this callback.
///
template <typename Func>
void Sweep::addCallback(Func func,
                        Callback::Target target,
                        Callback::Type type,
                        Callback::Marker marker)
{
    callbacks_.push_back(Callback(func, target, type, marker));
}

template
void Sweep::addCallback<Callback::HostFunc>(
    Callback::HostFunc host_func,
    Callback::Target target,
    Callback::Type type,
    Callback::Marker marker);

template
void Sweep::addCallback<Callback::DevFunc>(
    Callback::DevFunc device_func,
    Callback::Target target,
    Callback::Type type,
    Callback::Marker marker);

//------------------------------------------------------------------------------
/// \brief
///     Adds a callback to the list.
///     Allows to set the marker.
///     Sets the type to Other.
///
template <typename Func>
void Sweep::addCallback(Func func,
                        Callback::Target target,
                        Callback::Marker marker)
{
    callbacks_.push_back(Callback(func, target, Callback::Type::Other, marker));
}

template
void Sweep::addCallback<Callback::HostFunc>(
    Callback::HostFunc host_func,
    Callback::Target target,
    Callback::Marker marker);

template
void Sweep::addCallback<Callback::DevFunc>(
    Callback::DevFunc device_func,
    Callback::Target target,
    Callback::Marker marker);

//------------------------------------------------------------------------------
/// \brief
///     Reads the input CSV file.
///     Contains a quick implementation with no regard to performance
///     or checking the correctness of the input file.
///
void Sweep::readInput()
{
    Trace::Block trace_block("read input");

    // Open the file for reading.
    std::ifstream ifs;
    ifs.open(input_filename_);
    assert(ifs.is_open());

    // Read parameters' names.
    std::string names_line;
    std::getline(ifs, names_line);
    // Remove spaces.
    std::string::iterator end_pos = std::remove(names_line.begin(), names_line.end(), ' ');
    names_line.erase(end_pos, names_line.end());
    std::istringstream names_iline(names_line);

    std::string name;
    while (std::getline(names_iline, name, ','))
        param_names_.push_back(name);

    // Read parameters' types.
    std::string types_line;
    std::getline(ifs, types_line);
    // Remove spaces.
    end_pos = std::remove(types_line.begin(), types_line.end(), ' ');
    types_line.erase(end_pos, types_line.end());
    std::istringstream types_iline(types_line);

    std::string type;
    while (std::getline(types_iline, type, ','))
        if (type == "Real")
            param_types_.push_back(Value::Type::Real);
        else if (type == "Integer")
            param_types_.push_back(Value::Type::Integer);
        else if (type == "String")
            param_types_.push_back(Value::Type::String);

    // Read parameters' kinds.
    std::string kinds_line;
    std::getline(ifs, kinds_line);
    // Remove spaces.
    end_pos = std::remove(kinds_line.begin(), kinds_line.end(), ' ');
    kinds_line.erase(end_pos, kinds_line.end());
    std::istringstream kinds_iline(kinds_line);

    std::string kind;
    // no Output parameters in the input file
    while (std::getline(kinds_iline, kind, ','))
        if (kind == "Compile")
            param_kinds_.push_back(Param::Kind::Compile);
        else if (kind == "Runtime")
            param_kinds_.push_back(Param::Kind::Runtime);

    // Read parameters' values.
    param_vals_.resize(param_names_.size());
    std::string vals_line;
    while (std::getline(ifs, vals_line)) {
        // Remove spaces
        end_pos = std::remove(vals_line.begin(), vals_line.end(), ' ');
        vals_line.erase(end_pos, vals_line.end());
        std::istringstream vals_iline(vals_line);
        std::string val_string;
        int vals_count = 0;
        while (std::getline(vals_iline, val_string, ',')) {
            Value val;
            switch (param_types_[vals_count]) {
                case Value::Type::Real:
                    val.real = std::stod(val_string);
                    break;
                case Value::Type::Integer:
                    val.integer = std::stoll(val_string);
                    break;
                case Value::Type::String:
                    std::strncpy(val.string, val_string.c_str(),
                                 Value::MaxStringSize);
                    break;
            }
            param_vals_[vals_count++].push_back(val);
        }
    }
    // Close the file.
    ifs.close();
}

//------------------------------------------------------------------------------
/// \brief
///     Broadcasts the input data to all the ranks.
///     When the broadcast is finished, all ranks have a copy of all the data.
///
///     This function also creates the list of the input parameters' names.
///     This is because there is only one list for all the parameters, both
///     input and output, but only input parameters are used in the compile
///     line.
///
void Sweep::bcastInput()
{
    Trace::Block trace_block("broadcast input");

    // Broadcast the number of parameters.
    int num_params = param_names_.size();
    mpi_call(MPI_Bcast(&num_params, 1, MPI_INT, mpi_root_, mpi_comm_));

    // Resize the vectors.
    param_names_.resize(num_params);
    param_types_.resize(num_params);
    param_kinds_.resize(num_params);
    param_vals_.resize(num_params);

    // Broadcast the number of rows.
    num_rows_ = param_vals_[0].size();
    mpi_call(MPI_Bcast(&num_rows_, 1, MPI_LONG_LONG, mpi_root_, mpi_comm_));

    // Broadcast the types.
    mpi_call(MPI_Bcast(&param_types_[0], sizeof(Value::Type)*num_params,
                       MPI_BYTE, mpi_root_, mpi_comm_));

    // Broadcast the kinds.
    mpi_call(MPI_Bcast(&param_kinds_[0], sizeof(Param::Kind)*num_params,
                       MPI_BYTE, mpi_root_, mpi_comm_));

    for (int param = 0; param < num_params; ++param) {

        // Broadcast the name's length.
        int name_length = param_names_[param].length();
        mpi_call(MPI_Bcast(&name_length, 1, MPI_INT, mpi_root_, mpi_comm_));

        // Resize and broadcast the name.
        param_names_[param].resize(name_length);
        mpi_call(MPI_Bcast(&param_names_[param][0], name_length, MPI_CHAR,
                           mpi_root_, mpi_comm_));

        // Resize and broadcast the parameter's values.
        param_vals_[param].resize(num_rows_);
        mpi_call(MPI_Bcast(&param_vals_[param][0], sizeof(Value)*num_rows_,
                           MPI_BYTE, mpi_root_, mpi_comm_));
    }
    // Create the list of original input parameters.
    for (int param = 0; param < num_params; ++param)
        in_param_names_.insert(param_names_[param]);
}

//------------------------------------------------------------------------------
/// \brief
///     Adds an output parameter to the array of parameters.
///
/// \param[in] param_name
///     the name of the new parameter
///
/// \param[in] param_type
///     the type of the new parameter: Real, Integer, or String
///
void Sweep::addParameter(std::string param_name, Value::Type param_type)
{
    // Add the name to the vector of names.
    param_names_.push_back(param_name);

    // Add the type to the vector of types.
    param_types_.push_back(param_type);

    // Add the kind to the vector of kinds.
    param_kinds_.push_back(Param::Kind::Output);

    // Add a new column of values.
    int num_params = param_names_.size();
    param_vals_.resize(num_params);

    // Resize the new column of values.
    param_vals_[num_params-1].resize(num_rows_);
}

//------------------------------------------------------------------------------
/// \brief
///     Creates a key-value map of parameters for one row.
///
/// \param[in] idx
///     row index
///
/// \returns
///     the key-value map of parameters
///
KernelParams Sweep::getParamMap(int idx) const
{
    KernelParams kernel_params;
    for (std::size_t param = 0; param < param_names_.size(); ++param)
        kernel_params[param_names_[param]] = param_vals_[param][idx];

    return kernel_params;
}

//------------------------------------------------------------------------------
/// \brief
///     Creates the parameters' string for the compile line.
///
/// \param[in] idx
///     row index
///
/// \returns
///     the parameters' string
///
std::string Sweep::getCompileString(int idx) const
{
    std::string str = compile_flags_ + " ";
    for (std::size_t param = 0; param < param_names_.size(); ++param) {

        const std::string& name = param_names_[param];
        if (in_param_names_.find(name) != in_param_names_.end()) {

            str += "-D" + param_names_[param] + "=";
            switch (param_types_[param]) {
                case Value::Type::Real:
                    str += std::to_string(param_vals_[param][idx].real);
                    break;
                case Value::Type::Integer:
                    str += std::to_string(param_vals_[param][idx].integer);
                    break;
                case Value::Type::String:
                    str += param_vals_[param][idx].toString();
                    break;
            }
            str += " ";
        }
    }
    return str;
}

//------------------------------------------------------------------------------
/// \brief
///     Updates the parameters' values in one row of the main array.
///
/// \param[in] kernel_params
///     key-value map of parameters containing new values
///
/// \param[in] idx
///     row index
///
void Sweep::updateParams(const KernelParams& kernel_params, int idx)
{
    for (auto const& prm : kernel_params) {

        std::string const& name = prm.first;
        Value const& val = prm.second;

        for (std::size_t param = 0; param < param_names_.size(); ++param)
            if (name == param_names_[param])
                param_vals_[param][idx] = val;
    }
}

//------------------------------------------------------------------------------
/// \brief
///     Prints the parameters' names and values to the standard output.
///     This function is meant for debugging.
///
void Sweep::printParams() const
{
    for (int rank = 0; rank < mpi_size_; ++rank) {
        if (mpi_rank_ == rank) {

            // Print rank.
            std::cout << mpi_rank_ << std::endl;

            // Print names.
            for (auto const& name : param_names_)
                std::cout << name << " ";
            std::cout << std::endl;

            // Print types.
            for (auto const& type : param_types_)
                std::cout << Value::typeName(type) << " ";
            std::cout << std::endl;

            // Print kinds.
            for (auto const& kind : param_kinds_)
                std::cout << Param::kindName(kind) << " ";
            std::cout << std::endl;

            // Print values.
            int vals_count = 0;
            for (auto const& column : param_vals_) {
                for (auto const& val : column) {
                    val.print(std::cout, param_types_[vals_count]);
                    std::cout << " ";
                }
                std::cout << std::endl;
                ++vals_count;
            }
            std::cout << std::endl;
        }
        // Barrier and sleep to prevent scrambling of outputs
        // from multiple ranks.
        mpi_call(MPI_Barrier(mpi_comm_));
        usleep(100000);
    }
}

//------------------------------------------------------------------------------
/// \brief
///     Requests a chunk of work from the master process by sending a dummy
///     message with one byte of data.
///
///     In principle, we can send a zero size message, but it just feels wrong.
///
void Sweep::requestChunk() const
{
    Trace::Block trace_block("MPI_Send");
    char byte;
    mpi_call(MPI_Send(&byte, 1, MPI_BYTE, mpi_root_, mpi_tag_, mpi_comm_));
}

//------------------------------------------------------------------------------
/// \brief
///     Receives a chunk of work from the master process.
///
/// \returns
///     the index of the first row in the chunk
///
int64_t Sweep::receiveChunk() const
{
    Trace::Block trace_block("MPI_Recv");
    int64_t chunk_start;
    mpi_call(MPI_Recv(&chunk_start, 1, MPI_LONG_LONG, mpi_root_,
                      mpi_tag_, mpi_comm_, MPI_STATUS_IGNORE));
    return chunk_start;
}

//------------------------------------------------------------------------------
/// \brief
///     Processes a chunk of work.
///
/// \param[in] chunk_start
///     the index of the first row in the chunk
///
void Sweep::processChunk(int64_t chunk_start)
{
    for (int64_t row = chunk_start;
         row < chunk_start+chunk_size_ && row < num_rows_;
         ++row)
    {
        #pragma omp task
        {
            runRow(row);
        }
    }
    #pragma omp taskwait
}

//------------------------------------------------------------------------------
/// \brief
///     Receives a request for a chunk of work, which is a dummy message with
///     one byte of data.
///
///     In principle, we can receive a zero size message,
///     but it just feels wrong.
///
/// \returns
///     the rank of the process requesting work
///
int Sweep::recvChunkRequest() const
{
    Trace::Block trace_block("MPI_Recv");
    char byte;
    MPI_Status recv_status;
    mpi_call(MPI_Recv(&byte, 1, MPI_BYTE, MPI_ANY_SOURCE,
                      mpi_tag_, mpi_comm_, &recv_status));
    return recv_status.MPI_SOURCE;
}

//------------------------------------------------------------------------------
/// \brief
///     Sends a response to a request for work.
///
/// \param[in] chunk_start
///     the index of the first row in the chunk
///
/// \param[in] requester
///     rank of the process requesting work
///
void Sweep::sendChunkResponse(int64_t chunk_start, int requester) const
{
    Trace::Block trace_block("MPI_Send");
    mpi_call(MPI_Send(&chunk_start, 1, MPI_LONG_LONG, requester,
                      mpi_tag_, mpi_comm_));
}

//------------------------------------------------------------------------------
/// \brief
///     Look for cycle markers.
///     If not found, mark Kernel as the cycle.
///
void Sweep::findCycle()
{
    bool start_found = false;
    bool stop_found = false;
    for (auto const& callback : callbacks_) {
        if (callback.isStartCycle())
            start_found = true;
        if (callback.isStopCycle())
            stop_found = true;
    }

    // If both found, return.
    if (start_found && stop_found)
        return;

    // Check that both are missing.
    assert(!start_found && !stop_found);

    // Mark kernel as cycle.
    bool kernel_found = false;
    for (auto& callback : callbacks_) {
        if (callback.isKernel()) {
            callback.setStartStopCycle();
            kernel_found = true;
        }
    }
    // Check that kernel found.
    assert(kernel_found);
}

//------------------------------------------------------------------------------
/// \brief
///     Look for timer markers.
///     If not found, mark Kernel as the timer block.
///
void Sweep::findTimer()
{
    bool start_found = false;
    bool stop_found = false;
    for (auto const& callback : callbacks_) {
        if (callback.isStartTimer())
            start_found = true;
        if (callback.isStopTimer())
            stop_found = true;
    }

    // If both found, return.
    if (start_found && stop_found)
        return;

    // Check that both are missing.
    assert(!start_found && !stop_found);

    // Mark kernel as timer block.
    bool kernel_found = false;
    for (auto& callback : callbacks_) {
        if (callback.isKernel()) {
            callback.setStartStopTimer();
            kernel_found = true;
        }
    }
    // Check that kernel found.
    assert(kernel_found);
}

//------------------------------------------------------------------------------
/// \brief
///     Scan the list of callbacks.
///     Collect the callbacks in the cycle.
///     Catch illegal cleanup callbacks.
///     Exclude test callbacks.
///
/// \param[out] cycle_callbacks
///     list of callbacks in the cycle
///
void Sweep::collectCycleCallbacks(std::list<Callback>& cycle_callbacks) const
{
    // The range-based for preserves the order.
    // https://stackoverflow.com/questions/19052026/
    // will-range-based-for-loop-in-c-preserve-the-index-order

    bool inside_cycle = false;
    for (auto const& callback : callbacks_)
    {
        if (callback.isStartCycle())
            inside_cycle = true;

        // Catch illegal cleanup callbacks.
        if (inside_cycle)
            assert(!callback.isCleanup());

        // Collect the callbacks in the cycle.
        // Exclude test callbacks.
        if (inside_cycle && (!callback.isTest()))
            cycle_callbacks.push_back(callback);

        if (callback.isStopCycle())
            break;
    }
}

//------------------------------------------------------------------------------
/// \brief
///     Scan the list of callbacks.
///     Move the cleanup callbacks to the cleaup list.
///
/// \param[out] cleanup_callbacks
///     list of cleanup callbacks
///
void Sweep::moveCleanupCallbacks(std::list<Callback>& cleanup_callbacks)
{
    // https://stackoverflow.com/questions/596162/
    // can-you-remove-elements-from-a-stdlist-while-iterating-through-it

    auto i = callbacks_.begin();
    while (i != callbacks_.end()) {
        if (i->isCleanup()) {
            cleanup_callbacks.splice(
                cleanup_callbacks.end(), callbacks_, i++);
        }
        else {
            ++i;
        }
    }
}

//------------------------------------------------------------------------------
/// \brief
///     Scan the list of callbacks backwards.
///     Find and mark the last device callback.
///     The reserved device is released after that callback.
///
void Sweep::markLastDeviceCallback()
{
    for (auto i = callbacks_.rbegin(); i != callbacks_.rend(); ++i) {
        if (i->isDevice()) {
            i->setLastDevice(true);
            break;
        }
    }
}

//------------------------------------------------------------------------------
/// \brief
///     Assembles the pipeline.
///     Finds the cycle block. If not found, marks the kernel.
///     Finds the timer block. If not found, marks the kernel.
///     Adds cycles to the pipeline.
///     Moves cleanups to the end.
///     Marks the last device callback.
///
void Sweep::assemblePipeline()
{
    // Find the cycle.
    // If not found, mark the kernel.
    findCycle();

    // Find the timer block.
    // If not found, mark the kernel.
    findTimer();

    // Build the list of cycle callbacks.
    // Exclude test callbacks.
    std::list<Callback> cycle_callbacks;
    collectCycleCallbacks(cycle_callbacks);

    // Move cleanup callbacks to a new list.
    std::list<Callback> cleanup_callbacks;
    moveCleanupCallbacks(cleanup_callbacks);

    // Add cycle repetitions to the pipeline.
    for (int i = 0; i < num_reps_-1; ++i) {
        callbacks_.insert(callbacks_.end(),
                          cycle_callbacks.begin(), cycle_callbacks.end());
    }

    // Add cleanups to the pipeline.
    callbacks_.insert(callbacks_.end(),
                      cleanup_callbacks.begin(), cleanup_callbacks.end());

    // Mark the last device callback.
    // Device is released after this callback.
    markLastDeviceCallback();
}

//------------------------------------------------------------------------------
/// \brief
///     Reserves a device for exclusive access.
///     Looks through the list of devices until a free device is found.
///
/// \returns
///     the reserved device's number
///
int Sweep::reserveDevice()
{
    Trace::Block trace_block("devices busy");

    int dev;
    do {
        for (dev = 0; dev < num_devices_; ++dev)
            if (devices_[dev].reserve() == true)
                break;
    }
    while (dev == num_devices_);

    return dev;
}

//------------------------------------------------------------------------------
/// \brief
///     Executes one row of the autotuning sweep.
///
/// \param[in] idx
///     row index
///
void Sweep::runRow(std::size_t idx)
{
    KernelParams kernel_params = getParamMap(idx);
    std::string compile_string = getCompileString(idx);
    std::vector<char> callback_data(callback_data_size_);

    // Build the kernel.
    Kernel kernel(kernel_filename_, compile_string,
                  kernel_func_name_, scratch_path_, idx);
    {
        if (!kernel.compile(verbose_)) {
            // Set status and error.
            strcpy(kernel_params[std::string("Status")].string, "Failure");
            strcpy(kernel_params[std::string("Error")].string, "Compile");
            updateParams(kernel_params, idx);
            return;
        }
    }
    kernel.open();
    kernel.load();

    bool error_occured = false;
    Device* device_ptr = nullptr;

    double start_time;
    double elapsed_time;
    double min_time = std::numeric_limits<double>::infinity();

    // Execute the pipeline.
    for (auto const& callback : callbacks_) {

        // If error occured, only execute cleanup.
        if (error_occured && !callback.isCleanup())
            continue;

        // Start the timer.
        if (callback.isStartTimer())
            start_time = omp_get_wtime();

        bool success;
        if (callback.isHost()) {
            // Call host callback.
            success = callback.call(callback_data.data(), kernel_params);
        }
        else if (callback.isDevice()) {
            // If device not reserved, reserve and set.
            if (device_ptr == nullptr) {
                device_ptr = &devices_[reserveDevice()];
                device_ptr->set();
            }
            // If not kernel callback.
            if (!callback.isKernel()) {
                success = callback.call(callback_data.data(), kernel_params,
                                        device_ptr->stream());
            }
            // Else, if kernel callback.
            else {
                success = callback.call(callback_data.data(), kernel_params,
                                        device_ptr->stream(), kernel.func());
            }
            // If last device callback, release.
            if (callback.isLastDevice()) {
                device_ptr->release();
                device_ptr = nullptr;
            }
        }
        // Measure and update time.
        if (callback.isStopTimer()) {
            elapsed_time = omp_get_wtime()-start_time;
            if (elapsed_time < min_time) {
                min_time = elapsed_time;
                kernel_params[std::string("Time")].real = min_time;
            }
        }
        if (!success) {
            strcpy(kernel_params[std::string("Status")].string, "Failure");
            if (callback.isKernel())
                strcpy(kernel_params[std::string("Error")].string, "Launch");
            else if (callback.isTest())
                strcpy(kernel_params[std::string("Error")].string, "Test");
            else
                strcpy(kernel_params[std::string("Error")].string, "Other");
            error_occured = true;
        }
        updateParams(kernel_params, idx);
    }
    // If no errors, report success.
    if (!error_occured) {
        strcpy(kernel_params[std::string("Status")].string, "Success");
        strcpy(kernel_params[std::string("Error")].string, "None");
        updateParams(kernel_params, idx);
    }
    // If device not released, release.
    if (device_ptr != nullptr) {
        device_ptr->release();
        device_ptr = nullptr;
    }
    // Close and remove the kernel.
    kernel.close();
    kernel.remove();
}

//------------------------------------------------------------------------------
/// \brief
///     Prints the progress as percentage.
///     Relies on the progress of the master process.
///     Does not repeat the same number twice.
///
/// \param[in] chunk_start
///     beginning of the chunk to be processed next
///
void Sweep::printProgress(int chunk_start) const
{
    static int previous = 0;
    int percent = chunk_start*100/num_rows_;
    if (percent != previous)
        std::cout << percent << "%" << std::endl;

    previous = percent;
}

//------------------------------------------------------------------------------
/// \brief
///     Runs the autotuning sweep.
///
///     The first `omp master` block implements the dispatching of chunks to all
///     ranks other than the MPI root, and is only executed for MPI jobs.
///
///     The first `omp single` block implements the processing of chunks by the
///     MPI root, to be done concurrently with dispatching to other ranks.
///
///     The second `omp master` block implements the processing of chunks by all
///     MPI ranks, other than the MPI root.
///
void Sweep::run()
{
    assemblePipeline();

    if (mpi_rank_ == mpi_root_) {

        int64_t current_chunk = 0;
        #pragma omp parallel shared(current_chunk)
        {
            if (mpi_size_ > 1) {

                // MPI root's server code
                // executed by thread 0 with no wait
                #pragma omp master
                {
                    int64_t chunk_start;
                    do {
                        #pragma omp critical
                        {
                            // Atomically advance the chunk.
                            chunk_start = current_chunk;
                            current_chunk += chunk_size_;
                        }

                        if (chunk_start < num_rows_) {
                            // Service the next chunk request.
                            sendChunkResponse(chunk_start,
                                              recvChunkRequest());
                        }
                    }
                    while (chunk_start < num_rows_);

                    // Shut down the other ranks.
                    for (int i = 0; i < mpi_size_; ++i)
                        if (i != mpi_root_)
                            sendChunkResponse(-1, recvChunkRequest());
                }
            }

            // MPI root's client code
            // executed elsewhere, simultaneously
            #pragma omp single
            {
                int64_t chunk_start;
                do {
                    #pragma omp critical
                    {
                        // Atomically advance the chunk.
                        chunk_start = current_chunk;
                        current_chunk += chunk_size_;
                    }

                    if (chunk_start < num_rows_) {
                        processChunk(chunk_start);
                        printProgress(chunk_start);
                    }
                    else {
                        printProgress(num_rows_);
                    }
                }
                while (chunk_start < num_rows_);
            }
        }
    }
    else {
        // client code for other ranks
        // executed by thread 0
        #pragma omp parallel
        {
            #pragma omp master
            {
                int64_t chunk_start;
                do {
                    requestChunk();
                    chunk_start = receiveChunk();

                    if (chunk_start != -1)
                        processChunk(chunk_start);
                }
                while (chunk_start != -1);
            }
        }
    }
}

//------------------------------------------------------------------------------
/// \brief
///     Gather the output from all processes.
///
///     Applies bitwise OR to all values on all ranks.
///     This works because different ranks compute different rows.
///     It is also inefficient, becuase only one rank contains the result
///     for a particular row, and all other ranks contain zeros.
///     But it is simple and robust.
///
void Sweep::gatherOutput()
{
    Trace::Block trace_block("gather output");

    int num_params = param_names_.size();
    for (int param = 0; param < num_params; ++param) {

        std::string& name = param_names_[param];
        if (in_param_names_.find(name) == in_param_names_.end()) {

            void* buffer = &param_vals_[param][0];

            if (mpi_rank_ == mpi_root_) {
                MPI_Reduce(MPI_IN_PLACE, buffer, sizeof(Value)*num_rows_,
                           MPI_BYTE, MPI_BOR, mpi_root_, mpi_comm_);
            }
            else {
                MPI_Reduce(buffer, buffer, sizeof(Value)*num_rows_,
                           MPI_BYTE, MPI_BOR, mpi_root_, mpi_comm_);
            }
        }
    }
}

//------------------------------------------------------------------------------
/// \brief
///     Writes the results to a CSV file.
///     Contains a quick implementation with no regard to performance.
///
void Sweep::writeOutput() const
{
    Trace::Block trace_block("write output");

    std::ofstream ofs;
    ofs.open(output_filename_);
    assert(ofs.is_open());

    // Write parameters' names.
    int num_params = param_names_.size();
    for (int param = 0; param < num_params-1; ++param)
        ofs << param_names_[param] << ",";
    ofs << param_names_[num_params-1] << std::endl;

    // Write parameters' types.
    for (int param = 0; param < num_params-1; ++param)
        ofs << Value::typeName(param_types_[param]) << ",";
    ofs << Value::typeName(param_types_[num_params-1]) << std::endl;

    // Write parameters' kinds.
    for (int param = 0; param < num_params-1; ++param)
        ofs << Param::kindName(param_kinds_[param]) << ",";
    ofs << Param::kindName(param_kinds_[num_params-1]) << std::endl;

    // Write parameters' values.
    for (int64_t row = 0; row < num_rows_; ++row) {
        for (int param = 0; param < num_params-1; ++param) {
            param_vals_[param][row].print(ofs, param_types_[param]);
            ofs << ",";
        }
        param_vals_[num_params-1][row].print(ofs, param_types_[num_params-1]);
        ofs << std::endl;
    }
    ofs.close();
}

} // namespace bonsai

#endif // BONSAI_SWEEP_HH
