//------------------------------------------------------------------------------
// Copyright (c) 2018, University of Tennessee
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the University of Tennessee nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL UNIVERSITY OF TENNESSEE BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//------------------------------------------------------------------------------
// This material is based upon work supported by the National Science Foundation
// under Grant No. 1642441.
//------------------------------------------------------------------------------
// For assistance with BONSAI, email <bonsai-user@icl.utk.edu>.
//------------------------------------------------------------------------------

#ifndef BONSAI_EXCEPTION_HH
#define BONSAI_EXCEPTION_HH

#include <exception>
#include <string>

#include <cuda_runtime.h>
#include <mpi.h>

namespace bonsai {

//------------------------------------------------------------------------------
/// \class
/// \brief
///     Implements the base class for exceptions.
///
class Exception : public std::exception {
public:
    Exception()
        : std::exception()
    {}

    /// \brief
    ///     Sets the what() message to msg.
    Exception(std::string const& msg)
        : std::exception(),
          msg_(msg)
    {}

    /// \brief
    ///     Sets the what() message to msg with func, file, and line appended.
    Exception(std::string const& msg,
              const char* func, const char* file, int line)
        : std::exception(),
          msg_(msg + " in " + func + " at " + file + ":" + std::to_string(line))
    {}

    /// \brief
    ///     Returns a message describing the excecption.
    virtual char const* what() const noexcept override
    {
        return msg_.c_str();
    }

protected:
    /// \brief
    ///     Sets the what() message to msg with func, file, and line appended.
    void what(std::string const& msg,
              const char* func, const char* file, int line)
    {
        msg_ = msg + " in " + func + " at " + file + ":" + std::to_string(line);
    }

    std::string msg_;
};

/// Throws Exception with the given message.
#define error(msg) \
{ \
    throw Exception(msg, __func__, __FILE__, __LINE__); \
}

//------------------------------------------------------------------------------
/// \class
/// \brief
///     Implements the exception class for error_if().
///
class TrueConditionException : public Exception {
public:
    TrueConditionException(const char* cond,
                           const char* func,
                           const char* file,
                           int line)
        : Exception(std::string("BONSAI ERROR: Error condition '")
                    + cond + "' occured",
                    func, file, line)
    {}
};

/// Throws TrueConditionException if cond is true.
#define error_if(cond) \
{ \
    if (cond) \
        throw TrueConditionException(#cond, __func__, __FILE__, __LINE__); \
}

//------------------------------------------------------------------------------
/// \class
/// \brief
///     Implements the exception class for assert().
///
class FalseConditionException : public Exception {
public:
    FalseConditionException(const char* cond,
                            const char* func,
                            const char* file,
                            int line)
        : Exception(std::string("BONSAI ERROR: Error check '")
                    + cond + "' failed",
                    func, file, line)
    {}
};

/// Throws FalseConditionException if cond is false.
#define assert(cond) \
{ \
    if (!(cond)) \
        throw FalseConditionException(#cond, __func__, __FILE__, __LINE__); \
}

//------------------------------------------------------------------------------
/// \class
/// \brief
///     Implements the exception class for mpi_call().
///
class MpiException : public Exception {
public:
    MpiException(const char* call,
                 int code,
                 const char* func,
                 const char* file,
                 int line)
        : Exception()
    {
        char string[MPI_MAX_ERROR_STRING] = "unknown error";
        int resultlen;
        MPI_Error_string(code, string, &resultlen);

        what(std::string("MPI ERROR in BONSAI: ")
             + call + " failed: " + string
             + " (" + std::to_string(code) + ")",
             func, file, line);
    }
};

/// Throws MpiException if call does not return MPI_SUCCESS.
#define mpi_call(call) \
{ \
    int mpi_call = call; \
    if (mpi_call != MPI_SUCCESS) \
        throw MpiException(#call, mpi_call, __func__, __FILE__, __LINE__); \
}

//------------------------------------------------------------------------------
/// \class
/// \brief
///     Implements the exception class for cuda_call().
///
class CudaException : public Exception {
public:
    CudaException(const char* call,
                  cudaError_t code,
                  const char* func,
                  const char* file,
                  int line)
        : Exception()
    {
        const char* name = cudaGetErrorName(code);
        const char* string = cudaGetErrorString(code);

        what(std::string("CUDA ERROR in BONSAI: ")
             + call + " failed: " + string
             + " (" + name + "=" + std::to_string(code) + ")",
             func, file, line);
    }
};

/// Throws CudaException if call does not return cudaSuccess.
#define cuda_call(call) \
{ \
    cudaError_t cuda_call = call; \
    if (cuda_call != cudaSuccess) \
        throw CudaException(#call, cuda_call, __func__, __FILE__, __LINE__); \
}

} // namespace bonsai

#endif // BONSAI_EXCEPTION_HH
