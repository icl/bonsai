
warp_size = 32
float_size = 4

# for compute capability 6.0
max_threads_per_block = 1024
max_shmem_per_block = 49152
max_regs_per_thread = 255
max_regs_per_block = 65536

# heuristic constraints
min_threads_per_block = 256
min_fmas_per_load = 64

print("DIM_M,DIM_N,BLK_M,BLK_N,BLK_K,DIM_M_A,DIM_N_A,DIM_M_B,DIM_N_B")
print("Integer,Integer,Integer,Integer,Integer,Integer,Integer,Integer,Integer")
print("Compile,Compile,Compile,Compile,Compile,Compile,Compile,Compile,Compile")

for dim_m in range(4, 64+1, 4):
  for dim_n in range(4, 64+1, 4):
    threads_per_block = dim_m*dim_n

    if threads_per_block > max_threads_per_block:
      continue

    if threads_per_block % warp_size != 0:
      continue

    if threads_per_block < min_threads_per_block:
      continue

    for blk_m in range(dim_m, 128+1, dim_m):
      for blk_n in range(dim_n, 128+1, dim_n):

        regs_per_thread = (blk_m/dim_m)*(blk_n/dim_n)
        if regs_per_thread > max_regs_per_thread:
          continue

        regs_per_block = regs_per_thread*threads_per_block
        if regs_per_block > max_regs_per_block:
          continue

        for dim_m_a in range(4, blk_m+1, 4):
          for dim_n_a in range(4, 64+1, 4):

            if dim_m_a*dim_n_a != dim_m*dim_n:
              continue

            for dim_m_b in range(4, 64+1, 4):
              for dim_n_b in range(4, blk_n+1, 4):

                if dim_m_b*dim_n_b != dim_m*dim_n:
                  continue

                for blk_k in range(4, 64+1, 4):

                    if blk_k%dim_n_a != 0:
                      continue

                    if blk_k%dim_m_b != 0:
                      continue

                    loads_per_block = blk_m*blk_k + blk_k*blk_n
                    shmem_per_block = loads_per_block*float_size
                    if shmem_per_block > max_shmem_per_block:
                      continue

                    fmas_per_block = blk_m*blk_n*blk_k
                    fmas_per_load = fmas_per_block/loads_per_block
                    if fmas_per_load < min_fmas_per_load:
                      continue

                    print(str(dim_m)+","+str(dim_n)+","+
                          str(blk_m)+","+str(blk_n)+","+str(blk_k)+","+
                          str(dim_m_a)+","+str(dim_n_a)+","+
                          str(dim_m_b)+","+str(dim_n_b))
