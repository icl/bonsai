
// Matrix multiplication kernel called by sgemm()
__global__
void sgemm_kernel(
    int M, int N, int K,
    float alpha, float *A, int lda,
                 float *B, int ldb,
    float beta,  float *C, int ldc)
{
    int idx = threadIdx.x;     // thread's row position in C
    int idy = threadIdx.y;     // thread's col position in C
    int idt = DIM_M*idy + idx; // thread's number
    int blx = blockIdx.x;      // block's row position
    int bly = blockIdx.y;      // block's col position
    int idxA = idt % DIM_M_A;  // thread's row position for loading A
    int idyA = idt / DIM_M_A;  // thread's col position for loading A
    int idxB = idt % DIM_M_B;  // thread's row position for loading B
    int idyB = idt / DIM_M_B;  // thread's col position for loading B

    __shared__ float sA[BLK_K][BLK_M];  // shared memory for A
    __shared__ float sB[BLK_N][BLK_K];  // shared memory for B
    float rC[BLK_N/DIM_N][BLK_M/DIM_M]; // registers for C

    int coord_A = (blx*BLK_M     + idyA*lda) + idxA;
    int coord_B = (bly*BLK_N*ldb + idyB*ldb) + idxB;

    #pragma unroll
    for (int j = 0; j < BLK_N/DIM_N; ++j)
        #pragma unroll
        for (int i = 0; i < BLK_M/DIM_M; ++i)
            rC[j][i] = 0.0;

    for (int kk = 0; kk < K; kk += BLK_K)
    {
        #pragma unroll
        for (int j = 0; j < BLK_K; j += DIM_N_A)
            #pragma unroll
            for (int i = 0; i < BLK_M; i += DIM_M_A)
                sA[j+idyA][i+idxA] = __ldg(&A[coord_A + (j*lda+i)]);

        #pragma unroll
        for (int j = 0; j < BLK_N; j += DIM_N_B)
            #pragma unroll
            for (int i = 0; i < BLK_K; i += DIM_M_B)
                sB[j+idyB][i+idxB] = __ldg(&B[coord_B + (j*ldb+i)]);

        __syncthreads();

        #pragma unroll
        for (int k = 0; k < BLK_K; ++k)
            #pragma unroll
            for (int j = 0; j < BLK_N/DIM_N; ++j)
                #pragma unroll
                for (int i = 0; i < BLK_M/DIM_M; ++i)
                    rC[j][i] += sA[k][i*DIM_M+idx] * sB[j*DIM_N+idy][k];

        __syncthreads();

        coord_A += BLK_K*lda;
        coord_B += BLK_K;
    }

    #pragma unroll
    for (int j = 0; j < BLK_N/DIM_N; ++j) {
        int coord_dCn = bly*BLK_N + j*DIM_N+idy;
        #pragma unroll
        for (int i = 0; i < BLK_M/DIM_M; ++i) {
            int offsetC = coord_dCn*ldc + blx*BLK_M;
            C[offsetC + i*DIM_M+idx] =
                alpha*rC[j][i] + beta*C[offsetC + i*DIM_M+idx];
        }
    }
}

typedef struct {
    int m;
    int n;
    int k;
    float alpha;
    float *d_A;
    int lda;
    float *d_B;
    int ldb;
    float beta;
    float *d_C;
    int ldc;
} KernelData;

// Matrix multiplication - Host code
// Matrix dimensions are assumed to be multiples of BLK_M, BLK_N, BLK_M.
extern "C"
cudaError_t sgemm(void* kernel_data, cudaStream_t stream)
{
    KernelData* kd = (KernelData*)kernel_data;
    int m       = kd->m;
    int n       = kd->n;
    int k       = kd->k;
    float alpha = kd->alpha;
    float *d_A  = kd->d_A;
    int lda     = kd->lda;
    float *d_B  = kd->d_B;
    int ldb     = kd->ldb;
    float beta  = kd->beta;
    float *d_C  = kd->d_C;
    int ldc     = kd->ldc;

    dim3 dimBlock(DIM_M, DIM_N);
    int dim_grid_m = m%BLK_M == 0 ? m/BLK_M : m/BLK_M+1;
    int dim_grid_n = n%BLK_N == 0 ? n/BLK_N : n/BLK_N+1;
    dim3 dimGrid(dim_grid_m, dim_grid_n);
    sgemm_kernel<<<dimGrid, dimBlock, 0, stream>>>(
        m, n, k,
        alpha, d_A, lda,
               d_B, ldb,
        beta,  d_C, ldc);
    return cudaPeekAtLastError();
}
