
#include "bonsai_Sweep.hh"

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <dlfcn.h>
#include <map>

#include <cuda_runtime.h>
#include <cuda_profiler_api.h>
#include <omp.h>

// the struct for passing params to the kernel
typedef struct {
    int m;
    int n;
    int k;
    float alpha;
    float *d_A;
    int lda;
    float *d_B;
    int ldb;
    float beta;
    float *d_C;
    int ldc;
} KernelData;

// the struct for passing params between callbacks
typedef struct {
    int m;
    int n;
    int k;
    size_t size_A;
    size_t size_B;
    size_t size_C;
    float* h_A;
    float* h_B;
    float* h_C;
    float* d_A;
    float* d_B;
    float* d_C;
} CallbackData;

int g_dim;

bool init_input(void* callback_data, bonsai::KernelParams& kernel_params);

bool copy_input(void* callback_data,
                bonsai::KernelParams& kernel_params,
                cudaStream_t stream);

bool call_kernel(void* callback_data,
                 bonsai::KernelParams& kernel_params,
                 cudaStream_t stream,
                 bonsai::Kernel::Func kernel_func);

bool copy_output(void* callback_data,
                 bonsai::KernelParams& kernel_params,
                 cudaStream_t stream);

bool free_mem(void* callback_data,
              bonsai::KernelParams& kernel_params,
              cudaStream_t stream);

bool test_results(void* callback_data, bonsai::KernelParams& kernel_params);

//------------------------------------------------------------------------------
// Drives an autotuning sweep for a simple sgemm kernel.
//
int main(int argc, char *argv[])
{
    if (argc != 4) {
        std::cout << std::endl
                  << "usage: driver matrix_size num_reps chunk_size"
                  << std::endl << std::endl << "  matrix_size : "
                  << "size of the matrices to multiply (m=n=k)"
                  << std::endl << "  num_reps    : "
                  << "how many reps per one kernel configuration"
                  << std::endl << "  chunk_size  : "
                  << "how many kernels sent to one node at a time"
                  << std::endl << std::endl;
        exit (EXIT_FAILURE);
    }

    g_dim = atoi(argv[1]);
    const int num_reps = atoi(argv[2]);
    const int chunk_size = atoi(argv[3]);

    try {
        using namespace bonsai;
        using Target = Callback::Target;
        using Type   = Callback::Type;
        // using Marker = Callback::Marker;

        // Create the sweep.
        Sweep sweep(
            std::string("input.csv"),    // input CSV filename
            std::string("output.csv"),   // output CSV filename
            std::string("kernel.cu"),    // kernel source filename
            std::string("sgemm"),        // kernel function name
            sizeof(CallbackData),        // size of callback data
            num_reps,                    // number of repetitions
            chunk_size,                  // dispatch chunk size
            std::string("-arch=sm_60")); // extra compilation flags

        // Add the Gflops parameter to the output.
        sweep.addParameter(std::string("Gflops"), Value::Type::Real);

        // Create the sequence of callbacks.
        sweep.addCallback(init_input,   Target::Host                 );
        sweep.addCallback(copy_input,   Target::Device               );
        sweep.addCallback(call_kernel,  Target::Device, Type::Kernel );
        sweep.addCallback(copy_output,  Target::Device               );
        sweep.addCallback(free_mem,     Target::Device, Type::Cleanup);
        sweep.addCallback(test_results, Target::Host  , Type::Test   );

        // Run the sweep.
        sweep.run();
    }
    catch (bonsai::Exception& e) {
        std::cerr << std::endl << e.what() << std::endl << std::endl;
    }
}

//------------------------------------------------------------------------------
// Rounds up the m, n, k dimensions to a multiples of BLK_M, BLK_N, BLK_K.
// Allocates A, B, and C in host memory.
// Sets all elements of A and B to 1.0.
// Sets all elements of C to 0.0.
//
bool init_input(void* callback_data, bonsai::KernelParams& kernel_params)
{
    CallbackData* cbd = (CallbackData*)callback_data;

    cbd->m = g_dim;
    cbd->n = g_dim;
    cbd->k = g_dim;

    uint64_t blk_m = kernel_params.at(std::string("BLK_M")).integer;
    uint64_t blk_n = kernel_params.at(std::string("BLK_N")).integer;
    uint64_t blk_k = kernel_params.at(std::string("BLK_K")).integer;

    if (cbd->m % blk_m != 0)
        cbd->m = (cbd->m/blk_m+1)*blk_m;

    if (cbd->n % blk_n != 0)
        cbd->n = (cbd->n/blk_n+1)*blk_n;

    if (cbd->k % blk_k != 0)
        cbd->k = (cbd->k/blk_k+1)*blk_k;

    cbd->size_A = sizeof(float)*cbd->m*cbd->k;
    cbd->size_B = sizeof(float)*cbd->k*cbd->n;
    cbd->size_C = sizeof(float)*cbd->m*cbd->n;

    cbd->h_A = (float*)malloc(cbd->size_A);
    cbd->h_B = (float*)malloc(cbd->size_B);
    cbd->h_C = (float*)malloc(cbd->size_C);

    assert(cbd->h_A != nullptr);
    assert(cbd->h_B != nullptr);
    assert(cbd->h_C != nullptr);

    for (int64_t i = 0; i < cbd->m*cbd->k; ++i) {
        cbd->h_A[i] = 1.0f;
    }
    for (int64_t i = 0; i < cbd->k*cbd->n; ++i) {
        cbd->h_B[i] = 1.0f;
    }
    memset(cbd->h_C, 0, cbd->size_C);

    return true;
}

//------------------------------------------------------------------------------
// Allocates A, B, and C in device memory.
// Copies A, B, and C from host memory to device memory.
//
bool copy_input(void* callback_data,
                bonsai::KernelParams& kernel_params,
                cudaStream_t stream)
{
    CallbackData* cbd = (CallbackData*)callback_data;

    cudaMalloc(&cbd->d_A, cbd->size_A);
    cudaMalloc(&cbd->d_B, cbd->size_B);
    cudaMalloc(&cbd->d_C, cbd->size_C);

    cudaMemcpyAsync(cbd->d_A, cbd->h_A, cbd->size_A,
                    cudaMemcpyHostToDevice, stream);

    cudaMemcpyAsync(cbd->d_B, cbd->h_B, cbd->size_B,
                    cudaMemcpyHostToDevice, stream);

    cudaMemcpyAsync(cbd->d_C, cbd->h_C, cbd->size_C,
                    cudaMemcpyHostToDevice, stream);

    return cudaGetLastError() == cudaSuccess;
}

//------------------------------------------------------------------------------
// Prepares the data block containing the kernel call arguments.
// Calls the kernel.
//
bool call_kernel(void* callback_data,
                 bonsai::KernelParams& kernel_params,
                 cudaStream_t stream,
                 bonsai::Kernel::Func kernel_func)
{
    CallbackData* cbd = (CallbackData*)callback_data;

    // Prepare the kernel data.
    KernelData* kd = new KernelData();
    kd->m = cbd->m;
    kd->n = cbd->n;
    kd->k = cbd->k;
    kd->alpha = 1.0f;
    kd->d_A = cbd->d_A;
    kd->lda = cbd->m;
    kd->d_B = cbd->d_B;
    kd->ldb = cbd->k;
    kd->beta = 1.0f;
    kd->d_C = cbd->d_C;
    kd->ldc = cbd->m;

    // Call the kernel.
    cudaError_t status = kernel_func(kd, stream);

    // Delete the kernel data.
    delete kd;

    return status == cudaSuccess;
}

//------------------------------------------------------------------------------
// Copies C from device memory to host memory.
//
bool copy_output(void* callback_data,
                 bonsai::KernelParams& kernel_params,
                 cudaStream_t stream)
{
    CallbackData* cbd = (CallbackData*)callback_data;

    cudaMemcpyAsync(cbd->h_C, cbd->d_C, cbd->size_C,
                    cudaMemcpyDeviceToHost, stream);

    return cudaGetLastError() == cudaSuccess;
}

//------------------------------------------------------------------------------
// Frees A, B, and C in device memory.
// Frees A, B, ane C in host memory.
// Computes the Gflops rate.
//
bool free_mem(void* callback_data,
              bonsai::KernelParams& kernel_params,
              cudaStream_t stream)
{
    CallbackData* cbd = (CallbackData*)callback_data;

    cudaFree(cbd->d_A);
    cudaFree(cbd->d_B);
    cudaFree(cbd->d_C);

    double elapsed = kernel_params.at(std::string("Time")).real;
    double gflops = 2.0*cbd->m*cbd->n*cbd->k / elapsed / 1e9;
    kernel_params[std::string("Gflops")].real = gflops;

    return cudaGetLastError() == cudaSuccess;
}

//------------------------------------------------------------------------------
// Tests the result.
//
bool test_results(void* callback_data, bonsai::KernelParams& kernel_params)
{
    CallbackData* cbd = (CallbackData*)callback_data;

    for (int64_t i = 0; i < cbd->m*cbd->n; ++i)
        if (cbd->h_C[i] != cbd->k)
            return false;

    free(cbd->h_A);
    free(cbd->h_B);
    free(cbd->h_C);

    return true;
}
