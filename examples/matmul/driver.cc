
#include "bonsai_Sweep.hh"

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <dlfcn.h>
#include <map>

#include <cuda_runtime.h>
#include <cuda_profiler_api.h>
#include <omp.h>

// the Matrix struct from the MatMul example
typedef struct {
    int width;
    int height;
    int stride;
    float* elements;
} Matrix;

// the struct for passing params to the kernel
typedef struct {
    Matrix d_A;
    Matrix d_B;
    Matrix d_C;
    cudaFuncCache cache_config;
    cudaSharedMemConfig shared_config;
} KernelData;

// the struct for passing params to the callbacks
typedef struct {
    int dim;
    size_t size;
    float* h_A;
    float* h_B;
    float* h_C;
    float* d_A;
    float* d_B;
    float* d_C;
} CallbackData;

int g_dim;

bool init_input(void* callback_data, bonsai::KernelParams& kernel_params);

bool copy_input(void* callback_data,
                bonsai::KernelParams& kernel_params,
                cudaStream_t stream);

bool call_kernel(void* callback_data,
                 bonsai::KernelParams& kernel_params,
                 cudaStream_t stream,
                 bonsai::Kernel::Func kernel_func);

bool copy_output(void* callback_data,
                 bonsai::KernelParams& kernel_params,
                 cudaStream_t stream);

bool free_mem(void* callback_data,
              bonsai::KernelParams& kernel_params,
              cudaStream_t stream);

bool test_results(void* callback_data, bonsai::KernelParams& kernel_params);

//------------------------------------------------------------------------------
// Drive the autotuning sweep for the MatMul kernel
// from the CUDA Programming Guide.
//
int main(int argc, char *argv[])
{
    if (argc != 4) {
        std::cout << std::endl
                  << "usage: driver matrix_size num_reps chunk_size"
                  << std::endl << std::endl << "  matrix_size : "
                  << "size of the matrices to multiply (m=n=k)"
                  << std::endl << "  num_reps    : "
                  << "how many reps per one kernel configuration"
                  << std::endl << "  chunk_size  : "
                  << "how many kernels sent to one node at a time"
                  << std::endl << std::endl;
        exit (EXIT_FAILURE);
    }

    g_dim = atoi(argv[1]);
    const int num_reps = atoi(argv[2]);
    const int chunk_size = atoi(argv[3]);

    try {
        using namespace bonsai;
        using Target = Callback::Target;
        using Type   = Callback::Type;
        // using Marker = Callback::Marker;

        // Create the sweep.
        Sweep sweep(
            std::string("input.csv"),    // input CSV filename
            std::string("output.csv"),   // output CSV filename
            std::string("kernel.cu"),    // kernel source filename
            std::string("MatMul"),       // kernel function name
            sizeof(CallbackData),        // size of callback data
            num_reps,                    // number of repetitions
            chunk_size,                  // dispatch chunk size
            std::string("-arch=sm_60")); // extra compilation flags

        // Add the Gflops parameter to the output.
        sweep.addParameter(std::string("Gflops"), Value::Type::Real);

        // Create the sequence of callbacks.
        sweep.addCallback(init_input,   Target::Host                 );
        sweep.addCallback(copy_input,   Target::Device               );
        sweep.addCallback(call_kernel,  Target::Device, Type::Kernel );
        sweep.addCallback(copy_output,  Target::Device               );
        sweep.addCallback(free_mem,     Target::Device, Type::Cleanup);
        sweep.addCallback(test_results, Target::Host  , Type::Test   );

        // Run the sweep.
        sweep.run();
    }
    catch (bonsai::Exception& e) {
        std::cerr << std::endl << e.what() << std::endl << std::endl;
    }
}

//------------------------------------------------------------------------------
// Rounds up the size to a multiple of BLOCK_SIZE.
// Allocates A, B, and C in host memory.
// Sets all elements of A and B to 1.0.
// Sets all elements of C to 0.0.
//
bool init_input(void* callback_data, bonsai::KernelParams& kernel_params)
{
    CallbackData* cbd = (CallbackData*)callback_data;
    uint64_t block_size = kernel_params.at(std::string("BLOCK_SIZE")).integer;

    cbd->dim = g_dim;
    if (cbd->dim % block_size != 0)
        cbd->dim = (cbd->dim/block_size+1)*block_size;

    cbd->size = sizeof(float)*cbd->dim*cbd->dim;

    cbd->h_A = (float*)malloc(cbd->size);
    cbd->h_B = (float*)malloc(cbd->size);
    cbd->h_C = (float*)malloc(cbd->size);

    assert(cbd->h_A != nullptr);
    assert(cbd->h_B != nullptr);
    assert(cbd->h_C != nullptr);

    for (int64_t i = 0; i < cbd->dim*cbd->dim; ++i) {
        cbd->h_A[i] = 1.0f;
        cbd->h_B[i] = 1.0f;
    }
    memset(cbd->h_C, 0, cbd->size);

    return true;
}

//------------------------------------------------------------------------------
// Allocates A, B, and C in device memory.
// Copies A, B, and C from host memory to device memory.
//
bool copy_input(void* callback_data,
                bonsai::KernelParams& kernel_params,
                cudaStream_t stream)
{
    CallbackData* cbd = (CallbackData*)callback_data;

    cudaMalloc(&cbd->d_A, cbd->size);
    cudaMalloc(&cbd->d_B, cbd->size);
    cudaMalloc(&cbd->d_C, cbd->size);

    cudaMemcpyAsync(cbd->d_A, cbd->h_A, cbd->size,
                    cudaMemcpyHostToDevice, stream);

    cudaMemcpyAsync(cbd->d_B, cbd->h_B, cbd->size,
                    cudaMemcpyHostToDevice, stream);

    cudaMemcpyAsync(cbd->d_C, cbd->h_C, cbd->size,
                    cudaMemcpyHostToDevice, stream);

    return cudaGetLastError() == cudaSuccess;
}

//------------------------------------------------------------------------------
// Prepares the data block containing the kernel call arguments.
// Calls the kernel.
//
bool call_kernel(void* callback_data,
                 bonsai::KernelParams& kernel_params,
                 cudaStream_t stream,
                 bonsai::Kernel::Func kernel_func)
{
    CallbackData* cbd = (CallbackData*)callback_data;

    // Prepare the kernel data.
    KernelData* kd = new KernelData();
    kd->d_A = {cbd->dim, cbd->dim, cbd->dim, cbd->d_A};
    kd->d_B = {cbd->dim, cbd->dim, cbd->dim, cbd->d_B};
    kd->d_C = {cbd->dim, cbd->dim, cbd->dim, cbd->d_C};
/*
    // Set cache config.
    char* cache_config = kernel_params[std::string("CacheConfig")].string;
    if (strcmp(cache_config, "Shared") == 0)
        kd->cache_config = cudaFuncCachePreferShared;
    else if (strcmp(cache_config, "L1") == 0)
        kd->cache_config = cudaFuncCachePreferL1;
    else
        assert(0);
*/
    // Set shared mem config.
    char* shared_config = kernel_params[std::string("SharedMemConfig")].string;
    if (strcmp(shared_config, "Four") == 0)
        kd->shared_config = cudaSharedMemBankSizeFourByte;
    else if (strcmp(shared_config, "Eight") == 0)
        kd->shared_config = cudaSharedMemBankSizeEightByte;
    else
        assert(0);

    // Call the kernel.
    cudaError_t status = kernel_func(kd, stream);

    // Delete the kernel data.
    delete kd;

    return status == cudaSuccess;
}

//------------------------------------------------------------------------------
// Copies C from device memory to host memory.
//
bool copy_output(void* callback_data,
                 bonsai::KernelParams& kernel_params,
                 cudaStream_t stream)
{
    CallbackData* cbd = (CallbackData*)callback_data;

    cudaMemcpyAsync(cbd->h_C, cbd->d_C, cbd->size,
                    cudaMemcpyDeviceToHost, stream);

    return cudaGetLastError() == cudaSuccess;
}

//------------------------------------------------------------------------------
// Frees A, B, and C in device memory.
// Frees A, B, ane C in host memory.
// Computes the Gflops rate.
//
bool free_mem(void* callback_data,
              bonsai::KernelParams& kernel_params,
              cudaStream_t stream)
{
    CallbackData* cbd = (CallbackData*)callback_data;

    cudaFree(cbd->d_A);
    cudaFree(cbd->d_B);
    cudaFree(cbd->d_C);

    double elapsed = kernel_params.at(std::string("Time")).real;
    double gflops = 2.0*cbd->dim*cbd->dim*cbd->dim / elapsed / 1000000000.0;
    kernel_params[std::string("Gflops")].real = gflops;

    return cudaGetLastError() == cudaSuccess;
}

//------------------------------------------------------------------------------
// Tests the result.
//
bool test_results(void* callback_data, bonsai::KernelParams& kernel_params)
{
    CallbackData* cbd = (CallbackData*)callback_data;

    for (int64_t i = 0; i < cbd->dim*cbd->dim; ++i)
        if (cbd->h_C[i] != cbd->dim)
            return false;

    free(cbd->h_A);
    free(cbd->h_B);
    free(cbd->h_C);

    return true;
}
